<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('acesso')->default('cliente');
            $table->string('cadastro');

            // PF
            $table->string('nome');
            $table->string('sobrenome');
            $table->string('cpf');

            //PJ
            $table->string('razao_social');
            $table->string('nome_fantasia');
            $table->string('cnpj');
            $table->string('inscricao_estadual');
            $table->string('responsavel');

            $table->string('telefone');
            $table->string('email')->unique();
            $table->string('password', 60);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
