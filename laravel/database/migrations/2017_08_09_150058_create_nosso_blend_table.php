<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNossoBlendTable extends Migration
{
    public function up()
    {
        Schema::create('nosso_blend', function (Blueprint $table) {
            $table->increments('id');
            $table->string('box_subtitulo');
            $table->text('box_titulo');
            $table->text('titulo_lateral');
            $table->text('texto');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->string('imagem_5');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('nosso_blend');
    }
}
