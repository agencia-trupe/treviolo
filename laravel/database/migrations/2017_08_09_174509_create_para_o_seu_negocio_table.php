<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParaOSeuNegocioTable extends Migration
{
    public function up()
    {
        Schema::create('para_o_seu_negocio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('box_subtitulo');
            $table->text('box_titulo');
            $table->text('titulo_lateral');
            $table->text('texto');
            $table->string('titulo_grande');
            $table->string('subtitulo_2');
            $table->text('texto_2');
            $table->string('subtitulo_3');
            $table->text('texto_3');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('para_o_seu_negocio');
    }
}
