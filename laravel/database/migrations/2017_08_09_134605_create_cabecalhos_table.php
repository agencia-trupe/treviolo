<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabecalhosTable extends Migration
{
    public function up()
    {
        Schema::create('cabecalhos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sobre');
            $table->string('para_o_seu_negocio');
            $table->string('processo_produtivo');
            $table->string('nosso_blend');
            $table->string('onde_encontrar');
            $table->string('contato');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cabecalhos');
    }
}
