<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolucoesTable extends Migration
{
    public function up()
    {
        Schema::create('solucoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('titulo');
            $table->text('frase');
            $table->string('link');
            $table->boolean('novo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('solucoes');
    }
}
