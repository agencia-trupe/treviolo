<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->string('subtitulo');
            $table->text('descricao');
            $table->string('capa');
            $table->string('video_tipo');
            $table->string('video_codigo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('videos');
    }
}
