<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->text('abertura_titulo');
            $table->text('abertura_texto');
            $table->string('abertura_link');
            $table->text('frase_background');
            $table->string('subtitulo_s');
            $table->text('titulo_s');
            $table->text('texto_s');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
