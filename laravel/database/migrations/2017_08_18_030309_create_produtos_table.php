<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produtos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('capa');
            $table->string('miniatura');
            $table->string('titulo');
            $table->string('subtitulo');
            $table->text('descricao');
            $table->integer('quantidade_minima');
            $table->foreign('produtos_categoria_id')->references('id')->on('produtos_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos');
        Schema::drop('produtos_categorias');
    }
}
