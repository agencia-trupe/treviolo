<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSobreTable extends Migration
{
    public function up()
    {
        Schema::create('sobre', function (Blueprint $table) {
            $table->increments('id');
            $table->string('box_subtitulo');
            $table->text('box_titulo');
            $table->text('texto');
            $table->text('titulo_inferior');
            $table->string('subtitulo_2');
            $table->text('titulo_2');
            $table->text('texto_2');
            $table->string('subtitulo_3');
            $table->text('texto_3');
            $table->string('subtitulo_4');
            $table->text('texto_4');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sobre');
    }
}
