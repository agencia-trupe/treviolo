<?php

use Illuminate\Database\Seeder;

class ProdutosCategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos_categorias')->insert([
            [
                'titulo' => 'Grãos Treviolo',
                'slug'   => 'graos-treviolo',
                'ordem'  => 0
            ],
            [
                'titulo' => 'Insumos e acessórios',
                'slug'   => 'insumos-e-acessorios',
                'ordem'  => 1
            ]
        ]);
    }
}
