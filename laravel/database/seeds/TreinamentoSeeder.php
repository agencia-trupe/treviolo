<?php

use Illuminate\Database\Seeder;

class TreinamentoSeeder extends Seeder
{
    public function run()
    {
        DB::table('treinamento')->insert([
            'texto' => 'Para mais informa&ccedil;&otilde;es:<br />Rua Caet&eacute;s, 527/541 - Perdizes - S&atilde;o Paulo - SP<br />Mande um e-mail para <a href="mailto:treviolo@treviolo.com.br"><strong>treviolo@treviolo.com.br</strong></a><br />ou ligue para a gente: <strong>(11) 3868 6999</strong>',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
        ]);
    }
}
