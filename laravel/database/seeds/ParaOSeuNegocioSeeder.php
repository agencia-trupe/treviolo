<?php

use Illuminate\Database\Seeder;

class ParaOSeuNegocioSeeder extends Seeder
{
    public function run()
    {
        DB::table('para_o_seu_negocio')->insert([
            'box_subtitulo' => 'PARA O SEU NEGÓCIO',
            'box_titulo' => 'Quando você oferece<br>o melhor café, conquista<br>o paladar e a admiração',
            'titulo_lateral' => 'Ofereça<br>Treviolo!',
            'texto' => '<p>A Treviolo é especialista em café e oferece um atendimento feito por gente disposta e bem informada, além de alta performance em máquinas de café, auxílio no treinamento do seu staff, suporte técnico ágil e rápida entrega e reposição dos produtos que você precisar. Escolher a Treviolo é levar aos seus clientes um blend único, mas não só isso: é compartilhar com eles a nossa experiência e a paixão que todos nós nutrimos pelo café.</p>',
            'titulo_grande' => 'Temos a solução ideal para o seu negócio',
            'subtitulo_2' => 'RESTAURANTES, PADARIAS E HOTÉIS',
            'texto_2' => '<h2>Ofereça a melhor<br><strong>experiência</strong></h2><p>O espresso perfeito é capaz de conquistar e de criar novos vínculos. Seja para revenda ou para hospitalidade, oferecer um café de alta qualidade a seus clientes demonstra cuidado, conhecimento e paixão pelo que se faz.</p><h3>MÁQUINAS SUPERAUTOMÁTICAS</h3><p>Feitas exclusivamente para o nosso blend, moem os grãos ao simples toque de um botão, preservando todas as suas características. Praticidade e rapidez oferecidas especialmente para a revenda do seu café espresso ou para uma excelente hospitalidade.</p><h3>SERVIÇO ESPECIALIZADO</h3><p>Na fazenda ou na cidade, temos um equipe capacitada para entender o jeito Treviolo de fazer e oferecer café, espalhando nossa cultura e todas as informações para que o seu café seja sempre perfeito.</p>',
            'subtitulo_3' => 'ESCRITÓRIO',
            'texto_3' => '<h2>Leve a Treviolo<br>para o seu <strong>dia a dia</strong></h2><p>O aroma de um Treviolo tem o poder de transformar um ambiente. E você pode oferecer essa experiência diariamente aos seus clientes, colaboradores - e também a você - com uma de nossas máquinas em seu escritório.</p><h3>ESCOLHA SERVIR BEM. ESCOLHA TREVIOLO.</h3>',
            'imagem_1' => '',
            'imagem_2' => '',
        ]);
    }
}
