<?php

use Illuminate\Database\Seeder;

class NossoBlendSeeder extends Seeder
{
    public function run()
    {
        DB::table('nosso_blend')->insert([
            'box_subtitulo' => 'O BLEND TREVIOLO',
            'box_titulo' => 'Equilíbrio<br>e personalidade<br>inesquecíveis',
            'titulo_lateral' => 'Este café<br>é Treviolo!',
            'texto' => '<p>Blend. Como o próprio nome diz, uma mistura, mas não apenas de sabores. De sensações e experiências produzidas por uma combinação de paixão e conhecimento sobre café. Uma combinação única, homogênea, resultado de um trabalho feito por mãos que também cuidam da natureza e conhecem profundamente a arte de compor um blend e pela tecnologia de máquinas automáticas superavançadas.</p>',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
            'imagem_5' => '',
        ]);
    }
}
