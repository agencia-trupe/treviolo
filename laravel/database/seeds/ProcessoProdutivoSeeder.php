<?php

use Illuminate\Database\Seeder;

class ProcessoProdutivoSeeder extends Seeder
{
    public function run()
    {
        DB::table('processo_produtivo')->insert([
            'box_subtitulo' => 'PROCESSO PRODUTIVO',
            'box_titulo' => 'Uma jornada<br>de conhecimento<br>e amor',
            'titulo_lateral' => 'Do grão à xícara<br>o processo mais<br>harmonioso',
            'texto' => '<h3>CAFEICULTURA</h3><p>Nós participamos de todas as etapas da criação do blend: desde a produção até a colheita e beneficiamento dos grãos, que colhidos, secados e beneficiados no estágio certo de maturação concentram acidez, sacarose, óleos, aromas e sabores na medida certa.</p><h3>BLENDS DO CAFÉ ESPRESSO TREVIOLO</h3><p>O nosso blend está intimamente ligado à ideia de acolher e cativar. Para chegar a ele, mais de 40 variedades de café originárias do Brasil, da Itália e de regiões africanas já foram plantadas na nossa fazenda e em fazendas parceiras.</p><h3>CAFÉ MOÍDO NA HORA</h3><p>A experiência só é plena quando o café é moído na hora em máquinas de alta tecnologia, feitas exclusivamente para aqueles grãos. Um convite a quem quiser entrar, se sentar e se conectar com os próprios sentidos.</p>',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
            'imagem_5' => '',
        ]);
    }
}
