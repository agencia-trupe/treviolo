<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'     => 'treviolo@treviolo.com.br',
            'endereco'  => 'RUA CAETÉS, 527/541, PERDIZES · SÃO PAULO-SP',
            'telefone'  => '(11) 3868 6999',
            'facebook'  => 'https://www.facebook.com/treviolo/',
            'instagram' => 'https://www.instagram.com/treviolo_cafe/',
            'linkedin'  => '#linkedin'
        ]);
    }
}
