<?php

use Illuminate\Database\Seeder;

class CabecalhosSeeder extends Seeder
{
    public function run()
    {
        DB::table('cabecalhos')->insert([
            'sobre' => '',
            'para_o_seu_negocio' => '',
            'processo_produtivo' => '',
            'nosso_blend' => '',
            'onde_encontrar' => '',
            'contato' => '',
        ]);
    }
}
