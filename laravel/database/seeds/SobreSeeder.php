<?php

use Illuminate\Database\Seeder;

class SobreSeeder extends Seeder
{
    public function run()
    {
        DB::table('sobre')->insert([
            'box_subtitulo' => 'PROPÓSITO',
            'box_titulo' => 'Paixão e conhecimento:<br><strong>o segredo do blend<br>perfeito para você</strong>',
            'texto' => '<p>Café é encontro, é instante, é paixão.<br>É uma pausa, um presente. Café é inspiração.<br>E o nosso café vem do amor que temos por aquilo<br>que fazemos e pelo ritual de aroma e sabor que<br>podemos proporcionar às pessoas.<br>Café é compartilhamento, é experiência.<br>É uma troca de ideias e de sabedoria.<br>No café, há conhecimento.<br>E é isso o que queremos espalhar para você,<br>junto de nossos grãos.</p>',
            'titulo_inferior' => 'Vivemos para conhecer o café<br>e espalhar, junto com os grãos,<br>o nosso conhecimento.',
            'subtitulo_2' => 'HISTÓRIA',
            'titulo_2' => 'Nossas décadas de<br><strong>dedicação e de sabor</strong>',
            'texto_2' => '<p>A Treviolo nasceu em 1992, mas muito anos antes a nossa família já se dedicava à sua maior paixão: a cafeicultura.</p><h3>AMOR À TERRA, AO PLANTIO, AOS GRÃOS<br>E AO CONHECIMENTO DO CAFÉ.</h3><p>Ensinamentos transmitidos de avô para filhos e netos que nos levaram até a matéria-prima ideal para criar o que há de melhor em textura, acidez, aroma e sabor. Uma dedicação que hoje se revela pela experiência que vem de dentro da xícara de um Treviolo.',
            'subtitulo_3' => 'QUEM SOMOS',
            'texto_3' => '<h2>Somos apaixonados por fazer as pessoas se apaixonarem por café espresso.</h2><p>Produtos e serviços, máquinas de alta performance e profissionais 100% dedicados. Resumindo? Trabalhamos para ser o melhor serviço em café espresso que você já experimentou. Nosso compromisso é entregar o que há de melhor para apoiar sua oferta de espressos e, assim, fazer da hospitalidade a maior aliada do seu crescimento.</p><h3>EXIJA SEMPRE O MELHOR CAFÉ EM SUAS XÍCARAS. ESCOLHA PELO SABOR.</h3>',
            'subtitulo_4' => 'CARREIRAS',
            'texto_4' => '<h2>Você divide essa mesma paixão pelo café? Então, é hora de a gente se falar</h2><p>Queremos compartilhar nosso dia a dia com profissionais curiosos, dedicados e, acima de tudo, que tenham como seu principal foco o desejo de se dedicar ao espresso e levar às pessoas o melhor café que elas já provaram. Envie aqui seus dados, sua experiência e seus objetivos. E torne-se um membro da Treviolo.</p>',
            'imagem_1' => '',
            'imagem_2' => '',
        ]);
    }
}
