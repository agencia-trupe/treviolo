<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(TreinamentoSeeder::class);
		$this->call(SobreSeeder::class);
		$this->call(ParaOSeuNegocioSeeder::class);
		$this->call(NossoBlendSeeder::class);
		$this->call(ProcessoProdutivoSeeder::class);
		$this->call(HomeSeeder::class);
		$this->call(CabecalhosSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(ProdutosCategoriasSeeder::class);

        Model::reguard();
    }
}
