<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'abertura_titulo' => 'Do grão à xícara o<br><strong>processo mais harmonioso.</strong>',
            'abertura_texto' => 'O nosso café sempre chega em formato de grãos aos clientes. Um padrão que seguimos à risca para garantir que o produto será moído na hora. O nosso blend carrega conhecimento, pesquisa e amor para uma experiência sensorial marcante.  Fazemos tudo isso para você receber bem, encantar e fazer seu negócio crescer.',
            'abertura_link' => '#',
            'frase_background' => 'Vivemos para conhecer o café e espalhar,<br><strong>junto com os grãos, o nosso conhecimento.</strong>',
            'subtitulo_s' => 'ESPRESSO COM S?',
            'titulo_s' => 'Sim, café espresso<br><strong>com “s” por favor!</strong>',
            'texto_s' => 'A bebida tem esse nome não por sair rápido da máquina, mas sim, eSpremida. Dessa maneira, criamos maior resistência à passagem da àgua e melhor é a extração de óleos aromáticos e outros atributos sensoriais internos do café. Espresso vem do italiano, com nome e receita padronizados no mundo todo.',
        ]);
    }
}
