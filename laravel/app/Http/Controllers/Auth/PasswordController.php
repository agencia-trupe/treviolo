<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $subject = 'Link para redefinição de senha';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validateSendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email'    => 'email|required',
        ], [
            'email.required'    => 'Preencha seu e-mail.',
            'email.email'       => 'Insira um endereço de e-mail válido.',
        ]);
    }

    protected function getSendResetLinkEmailSuccessResponse()
    {
        return redirect()->route('clube.esqueci')->with([
            'senhaRedefinida' => request('email')
        ]);
    }

    public function showResetForm(Request $request, $token = null)
    {
        $email = $request->input('email');

        return view('frontend.clube.redefinir', compact('email', 'token'));
    }

    protected function getResetValidationMessages()
    {
        return [
            'password.required'  => 'Preencha sua senha.',
            'password.min'       => 'Sua senha deve ter no mínimo :min caracteres.',
            'password.confirmed' => 'A confirmação de senha não confere',
        ];
    }

    protected function getResetSuccessResponse($response)
    {
        return redirect()->route('clube')->with('senhaRedefinidaComSucesso', true);
    }
}
