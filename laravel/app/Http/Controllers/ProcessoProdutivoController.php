<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ProcessoProdutivo;

class ProcessoProdutivoController extends Controller
{
    public function index()
    {
        $processoProdutivo = ProcessoProdutivo::first();

        return view('frontend.processo-produtivo', compact('processoProdutivo'));
    }
}
