<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EstabelecimentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Estabelecimento;

class EstabelecimentosController extends Controller
{
    public function index()
    {
        $registros = Estabelecimento::orderBy('id', 'DESC')->get();

        return view('painel.estabelecimentos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.estabelecimentos.create');
    }

    public function store(EstabelecimentosRequest $request)
    {
        try {

            $input = $request->all();

            Estabelecimento::create($input);

            return redirect()->route('painel.estabelecimentos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Estabelecimento $registro)
    {
        return view('painel.estabelecimentos.edit', compact('registro'));
    }

    public function update(EstabelecimentosRequest $request, Estabelecimento $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.estabelecimentos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Estabelecimento $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.estabelecimentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
