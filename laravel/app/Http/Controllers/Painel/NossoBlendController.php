<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NossoBlendRequest;
use App\Http\Controllers\Controller;

use App\Models\NossoBlend;

class NossoBlendController extends Controller
{
    public function index()
    {
        $registro = NossoBlend::first();

        return view('painel.nosso-blend.edit', compact('registro'));
    }

    public function update(NossoBlendRequest $request, NossoBlend $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = NossoBlend::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = NossoBlend::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = NossoBlend::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = NossoBlend::upload_imagem_4();
            if (isset($input['imagem_5'])) $input['imagem_5'] = NossoBlend::upload_imagem_5();

            $registro->update($input);

            return redirect()->route('painel.nosso-blend.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
