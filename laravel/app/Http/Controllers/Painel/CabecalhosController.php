<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CabecalhosRequest;
use App\Http\Controllers\Controller;

use App\Models\Cabecalhos;

class CabecalhosController extends Controller
{
    public function index()
    {
        $registro = Cabecalhos::first();

        return view('painel.cabecalhos.edit', compact('registro'));
    }

    public function update(CabecalhosRequest $request, Cabecalhos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['sobre'])) $input['sobre'] = Cabecalhos::upload_sobre();
            if (isset($input['para_o_seu_negocio'])) $input['para_o_seu_negocio'] = Cabecalhos::upload_para_o_seu_negocio();
            if (isset($input['processo_produtivo'])) $input['processo_produtivo'] = Cabecalhos::upload_processo_produtivo();
            if (isset($input['nosso_blend'])) $input['nosso_blend'] = Cabecalhos::upload_nosso_blend();
            if (isset($input['onde_encontrar'])) $input['onde_encontrar'] = Cabecalhos::upload_onde_encontrar();
            if (isset($input['contato'])) $input['contato'] = Cabecalhos::upload_contato();

            $registro->update($input);

            return redirect()->route('painel.cabecalhos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
