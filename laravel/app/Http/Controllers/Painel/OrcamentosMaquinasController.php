<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\OrcamentoMaquinas;

class OrcamentosMaquinasController extends Controller
{
    public function index()
    {
        $contatosrecebidos = OrcamentoMaquinas::orderBy('created_at', 'DESC')->get();

        return view('painel.orcamentos.maquinas.index', compact('contatosrecebidos'));
    }

    public function show(OrcamentoMaquinas $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.orcamentos.maquinas.show', compact('contato'));
    }

    public function destroy(OrcamentoMaquinas $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.orcamentos.maquinas.index')->with('success', 'Orçamento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir orçamento: '.$e->getMessage()]);

        }
    }

    public function toggle(OrcamentoMaquinas $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.orcamentos.maquinas.index')->with('success', 'Orçamento alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar orçamento: '.$e->getMessage()]);

        }
    }
}
