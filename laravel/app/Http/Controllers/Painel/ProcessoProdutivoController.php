<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProcessoProdutivoRequest;
use App\Http\Controllers\Controller;

use App\Models\ProcessoProdutivo;

class ProcessoProdutivoController extends Controller
{
    public function index()
    {
        $registro = ProcessoProdutivo::first();

        return view('painel.processo-produtivo.edit', compact('registro'));
    }

    public function update(ProcessoProdutivoRequest $request, ProcessoProdutivo $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = ProcessoProdutivo::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = ProcessoProdutivo::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = ProcessoProdutivo::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = ProcessoProdutivo::upload_imagem_4();
            if (isset($input['imagem_5'])) $input['imagem_5'] = ProcessoProdutivo::upload_imagem_5();

            $registro->update($input);

            return redirect()->route('painel.processo-produtivo.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
