<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ManuaisRequest;
use App\Http\Controllers\Controller;

use App\Models\Manual;

class ManuaisController extends Controller
{
    public function index()
    {
        $registros = Manual::ordenados()->get();

        return view('painel.manuais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.manuais.create');
    }

    public function store(ManuaisRequest $request)
    {
        try {

            $input = $request->all();
            if ($request->hasFile('arquivo')) $input['arquivo'] = Manual::upload_arquivo();

            Manual::create($input);

            return redirect()->route('painel.manuais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Manual $registro)
    {
        return view('painel.manuais.edit', compact('registro'));
    }

    public function update(ManuaisRequest $request, Manual $registro)
    {
        try {

            $input = $request->all();
            if ($request->hasFile('arquivo')) $input['arquivo'] = Manual::upload_arquivo();

            $registro->update($input);

            return redirect()->route('painel.manuais.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Manual $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.manuais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
