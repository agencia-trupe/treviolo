<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClientesRequest;
use App\Http\Controllers\Controller;

use App\Models\Cliente;

class ClientesController extends Controller
{
    public function index()
    {
        $registros = Cliente::ordenados()->get();

        return view('painel.clientes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.clientes.create');
    }

    public function store(ClientesRequest $request)
    {
        try {

            $input = $request->all();

            Cliente::create($input);

            return redirect()->route('painel.clientes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Cliente $registro)
    {
        return view('painel.clientes.edit', compact('registro'));
    }

    public function update(ClientesRequest $request, Cliente $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.clientes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Cliente $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.clientes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
