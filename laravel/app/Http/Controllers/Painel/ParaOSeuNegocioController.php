<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ParaOSeuNegocioRequest;
use App\Http\Controllers\Controller;

use App\Models\ParaOSeuNegocio;

class ParaOSeuNegocioController extends Controller
{
    public function index()
    {
        $registro = ParaOSeuNegocio::first();

        return view('painel.para-o-seu-negocio.edit', compact('registro'));
    }

    public function update(ParaOSeuNegocioRequest $request, ParaOSeuNegocio $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = ParaOSeuNegocio::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = ParaOSeuNegocio::upload_imagem_2();

            $registro->update($input);

            return redirect()->route('painel.para-o-seu-negocio.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
