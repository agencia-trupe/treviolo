<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\NossoBlend;

class NossoBlendController extends Controller
{
    public function index()
    {
        $nossoBlend = NossoBlend::first();

        return view('frontend.nosso-blend', compact('nossoBlend'));
    }
}
