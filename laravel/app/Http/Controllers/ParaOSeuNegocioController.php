<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ParaOSeuNegocio;

class ParaOSeuNegocioController extends Controller
{
    public function index()
    {
        $paraOSeuNegocio = ParaOSeuNegocio::first();

        return view('frontend.para-o-seu-negocio', compact('paraOSeuNegocio'));
    }
}
