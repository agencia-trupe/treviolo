<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Solucao;
use App\Models\Cliente;
use App\Models\Home;

use App\Http\Requests\NewsletterRequest;
use App\Models\Newsletter;

class HomeController extends Controller
{
    public function index()
    {
        $banners  = Banner::ordenados()->get();
        $solucoes = Solucao::ordenados()->get();
        $clientes = Cliente::ordenados()->get();
        $home     = Home::first();

        return view('frontend.home', compact('banners', 'solucoes', 'clientes', 'home'));
    }

    public function newsletter(NewsletterRequest $request, Newsletter $newsletter)
    {
        $newsletter->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => 'cadastro efetuado com sucesso!'
        ];

        return response()->json($response);
    }
}
