<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\OrcamentosMaquinasRequest;

use Auth;
use App\Models\User;

use App\Models\Treinamento;
use App\Models\Manual;

use App\Models\Contato;
use App\Models\OrcamentoMaquinas;

use App\Models\Video;

class ClubeController extends Controller
{
    public function index()
    {
        return view('frontend.clube.index');
    }

    public function login()
    {
        if (Auth::user()) {
            if (Auth::user()->acesso == 'administrador') {
                Auth::logout();
            } else {
                return redirect()->route('clube');
            }
        }
        return view('frontend.clube.login');
    }

    public function loginPost(Request $request)
    {
        $this->validate($request, [
            'email'    => 'email|required',
            'password' => 'required'
        ], [
            'email.required'    => 'Preencha seu e-mail.',
            'email.email'       => 'Insira um endereço de e-mail válido.',
            'password.required' => 'Preencha sua senha.'
        ]);

        if (Auth::attempt([
            'email'    => $request->input('email'),
            'password' => $request->input('password')
        ], true)) {
            return redirect()->route('clube');
        } else {
            return redirect()->route('clube.login')->withInput()->withErrors(['Usuário ou senha inválidos.']);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }

    public function cadastro()
    {
        return view('frontend.clube.cadastro');
    }

    public function cadastroPost(Request $request)
    {
        if (!$request->get('cadastro')) abort('404');

        if ($request->get('cadastro') === 'pessoa_fisica') {
            $rules = [
                'nome'      => 'required',
                'sobrenome' => 'required',
                'cpf'       => 'required|cpf',
                'telefone'  => 'required',
                'email'     => 'required|email|unique:users',
                'password'  => 'required|confirmed|min:6',
            ];
        } else {
            $rules = [
                'razao_social'       => 'required',
                'nome_fantasia'      => 'required',
                'cnpj'               => 'required|cnpj',
                'inscricao_estadual' => 'required',
                'responsavel'        => 'required',
                'telefone'           => 'required',
                'email'              => 'required|email|unique:users',
                'password'           => 'required|confirmed|min:6',
            ];
        }

        $this->validate($request, $rules,
        [
            'nome.required'               => 'Preencha seu nome.',
            'sobrenome.required'          => 'Preencha seu sobrenome.',
            'cpf.required'                => 'Preencha seu CPF.',
            'cpf.cpf'                     => 'Insira um CPF válido',
            'razao_social.required'       => 'Preencha sua razão social.',
            'nome_fantasia.required'      => 'Preencha seu nome fantasia.',
            'cnpj.required'               => 'Preencha seu CNPJ.',
            'cnpj.cnpj'                   => 'Insira um CNPJ válido.',
            'inscricao_estadual.required' => 'Preencha sua inscrição estadual.',
            'responsavel.required'        => 'Preencha o responsável.',
            'telefone.required'           => 'Preencha seu telefone.',
            'email.required'              => 'Preencha seu e-mail.',
            'email.email'                 => 'Insira um endereço de e-mail válido.',
            'email.unique'                => 'O e-mail inserido já está cadastrado.',
            'password.required'           => 'Preencha sua senha.',
            'password.min'                => 'Sua senha deve ter no mínimo :min caracteres.',
            'password.confirmed'          => 'A confirmação de senha não confere',
        ]);

        try {

            $input = $request->except('password_confirmation', 'acesso');
            $input['password'] = bcrypt($input['password']);

            $usuario = User::create($input);

            Auth::login($usuario);

            return redirect()->route('clube');

        } catch (\Exception $e) {

            return back()->withInput()->withErrors(['Erro interno do servidor. Tente novamente.']);

        }
    }

    public function esqueci()
    {
        return view('frontend.clube.esqueci');
    }

    public function maquinas()
    {
        return view('frontend.clube.maquinas');
    }

    public function treinamento()
    {
        $treinamento = Treinamento::first();

        return view('frontend.clube.treinamento', compact('treinamento'));
    }

    public function manuais()
    {
        $manuais = Manual::ordenados()->get();

        return view('frontend.clube.manuais', compact('manuais'));
    }

    public function orcamentoMaquinas()
    {
        return view('frontend.clube.orcamento-maquinas');
    }

    public function orcamentoMaquinasPost(OrcamentosMaquinasRequest $request)
    {
        OrcamentoMaquinas::create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.maquinas', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[ORÇAMENTO DE MÁQUINAS] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return redirect()->route('clube.orcamentoMaquinas')->with('enviado', true);
    }

    public function videos(Video $video)
    {
        if (!$video->exists) {
            $video = Video::ordenados()->first();
        }

        $ultimos = Video::ordenados()->where('id', '!=', $video->id)->take(3)->get();

        return view('frontend.clube.videos', compact('video', 'ultimos'));
    }

    public function videosMais()
    {
        $videos = Video::ordenados()->get();

        return view('frontend.clube.videos-mais', compact('videos'));
    }
}
