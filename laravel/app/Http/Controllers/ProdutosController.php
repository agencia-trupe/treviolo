<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Produto;
use App\Models\ProdutoCategoria;

use App\Models\Orcamento;
use App\Models\Contato;

class ProdutosController extends Controller
{
    public function graos()
    {
        $categoria = ProdutoCategoria::whereSlug('graos-treviolo')->first();
        $produtos  = $categoria->produtos;

        return view('frontend.clube.produtos.index', compact('categoria', 'produtos'));
    }

    public function insumos()
    {
        $categoria = ProdutoCategoria::whereSlug('insumos-e-acessorios')->first();
        $produtos  = $categoria->produtos;

        return view('frontend.clube.produtos.index', compact('categoria', 'produtos'));
    }

    public function busca()
    {
        $termo = request('termo');

        if ($termo) {
            $produtos = Produto::ordenados()->busca($termo)->get();

            return view('frontend.clube.produtos.busca', compact('termo', 'produtos'));
        }

        return redirect()->route('clube');
    }

    public function checkout()
    {
        $orcamento = session()->get('orcamento', []);
        $produtos  = Produto::find(array_keys($orcamento));

        $produtos->map(function($produto) use ($orcamento) {
            $produto['quantidade'] = $orcamento[$produto->id];
            return $produto;
        });

        return view('frontend.clube.produtos.checkout', compact('produtos'));
    }

    public function adiciona(Request $request)
    {
        $produto = Produto::find($request->get('id'));

        if (!$produto) {
            return response()->json('Produto não encontrado.', 500);
        }

        $quantidade = (int)$request->get('quantidade');
        $minimo = $produto->quantidade_minima;

        if ($quantidade < $minimo) {
            return response()->json('A quantidade mínima deste produto é '.$minimo.'.', 500);
        }

        try {

            $orcamento = session()->get('orcamento', []);

            if (array_key_exists($produto->id, $orcamento)) {
                $orcamento[$produto->id] += $quantidade;
            } else {
                $orcamento[$produto->id] = $quantidade;
            }

            session()->put('orcamento', $orcamento);

            return response()->json([
                'orcamento' => $orcamento,
                'message'   => 'Produto adicionado com sucesso!'
            ]);

        } catch (\Exception $e) {

            return response()->json('Erro interno do servidor.', 500);

        }
    }

    public function atualiza(Request $request)
    {
        $produto = Produto::find($request->get('id'));

        if (!$produto) {
            return response()->json('Produto não encontrado.', 500);
        }

        try {

            $quantidade = (int)$request->get('quantidade');
            $minimo = $produto->quantidade_minima;

            if ($quantidade < $minimo) {
                return response()->json('A quantidade mínima deste produto é '.$minimo.'.', 500);
            }

            $orcamento = session()->get('orcamento', []);

            if (array_key_exists($produto->id, $orcamento)) {
                $orcamento[$produto->id] = $quantidade;
            }

            session()->put('orcamento', $orcamento);

            return response()->json([
                'message' => 'Orçamento atualizado com sucesso!'
            ]);

        } catch (\Exception $e) {

            return response()->json('Erro interno do servidor.', 500);

        }
    }

    public function exclui(Request $request)
    {
        $produto = Produto::find($request->get('id'));

        if (!$produto) {
            return response()->json('Produto não encontrado.', 500);
        }

        try {

            $orcamento = session()->get('orcamento', []);

            if (array_key_exists($produto->id, $orcamento)) {
                unset($orcamento[$produto->id]);
            }

            session()->put('orcamento', $orcamento);

            return response()->json([
                'message' => 'Produto removido com sucesso!'
            ]);

        } catch (\Exception $e) {

            return response()->json('Erro interno do servidor.', 500);

        }
    }

    public function pedido(Request $request)
    {
        $orcamento = session()->get('orcamento', []);
        $produtos  = Produto::find(array_keys($orcamento));

        $produtos->map(function($produto) use ($orcamento) {
            $produto['quantidade'] = $orcamento[$produto->id];
            return $produto;
        });

        if (!count($produtos)) {
            return redirect()->back();
        }

        $htmlOrcamento = '';
        foreach($produtos as $produto) {
            $htmlOrcamento .= "<p><strong>{$produto->titulo}";
            if ($produto->subtitulo) {
                $htmlOrcamento .= " &middot; {$produto->subtitulo}";
            }
            $htmlOrcamento .= "</strong> - Quantidade: {$produto->quantidade}</p>";
        }

        $orcamento = Orcamento::create([
            'user_id'   => \Auth::user()->id,
            'orcamento' => $htmlOrcamento,
        ]);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.orcamento', compact('orcamento'), function($message) use ($orcamento, $contato)
            {
                $nome = $orcamento->cliente->cadastro === 'pessoa_fisica' ? $orcamento->cliente->nome : $orcamento->cliente->nome_fantasia;

                $message->to($contato->email, config('site.name'))
                        ->subject('[ORÇAMENTO DE PRODUTOS] '.config('site.name'))
                        ->replyTo($orcamento->cliente->email, $nome);
            });
        }

        session()->forget('orcamento');

        return view('frontend.clube.produtos.enviado');
    }
}
