<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Estabelecimento;

use Location\Coordinate;
use Location\Distance\Haversine;

class EstabelecimentosController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('cep')) {
            if (!$this->cepValido($request->get('cep'))) {
                return redirect()->route('onde-encontrar');
            }

            $url = 'http://maps.google.com/maps/api/geocode/json?address='.$request->get('cep');

            try {
                $responseGoogle = file_get_contents($url);
            } catch (\Exception $e) {
                $input = $request->all();
                $erro  = 'Ocorreu um problema ao encontrar o CEP. Tente novamente.';

                return redirect()->route('onde-encontrar')->with('erro', $erro);
            }

            $responseGoogle = json_decode($responseGoogle);
            if (!count($responseGoogle->results)) {
                $input = $request->all();
                $erro  = 'CEP não encontrado. Tente novamente.';

                return redirect()->route('onde-encontrar')->with('erro', $erro);
            }

            $coordenadas = $responseGoogle->results[0]->geometry->location;

            $DISTANCIA_MAXIMA = 20000; // 20.000 KM

            $estabelecimentos = Estabelecimento::all()
                ->filter(function($estabelecimento) use ($coordenadas, $DISTANCIA_MAXIMA) {
                    $coordinate1 = new Coordinate($estabelecimento->lat, $estabelecimento->lng);
                    $coordinate2 = new Coordinate($coordenadas->lat, $coordenadas->lng);

                    return $coordinate1->getDistance($coordinate2, new Haversine()) < $DISTANCIA_MAXIMA;
                })
                ->sortBy(function($estabelecimento) use ($coordenadas) {
                    $coordinate1 = new Coordinate($estabelecimento->lat, $estabelecimento->lng);
                    $coordinate2 = new Coordinate($coordenadas->lat, $coordenadas->lng);

                    return $coordinate1->getDistance($coordinate2, new Haversine());
                });
        } else {
            $estabelecimentos = Estabelecimento::all();
        }

        return view('frontend.onde-encontrar', compact('estabelecimentos'));
    }

    private function cepValido($cep) {
        return preg_match("/^[0-9]{5}-[0-9]{3}$/", trim($cep)) || preg_match("/^[0-9]{8}$/", trim($cep));
    }
}
