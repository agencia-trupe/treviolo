<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('sobre', 'SobreController@index')->name('sobre');
    Route::get('para-o-seu-negocio', 'ParaOSeuNegocioController@index')->name('negocio');
    Route::get('o-processo-produtivo', 'ProcessoProdutivoController@index')->name('processo');
    Route::get('nosso-blend', 'NossoBlendController@index')->name('nosso-blend');
    Route::get('onde-encontrar', 'EstabelecimentosController@index')->name('onde-encontrar');
    Route::get('fale-com-a-gente', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');

    Route::group([
        'prefix'     => 'clube-treviolo',
        'middleware' => ['authCliente']
    ], function() {
        Route::get('/', 'ClubeController@index')->name('clube');

        Route::get('graos-treviolo', 'ProdutosController@graos')->name('clube.graos');
        Route::get('insumos-e-acessorios', 'ProdutosController@insumos')->name('clube.insumos');
        Route::get('checkout', 'ProdutosController@checkout')->name('clube.checkout');
        Route::post('adiciona-produto', 'ProdutosController@adiciona');
        Route::post('exclui-produto', 'ProdutosController@exclui');
        Route::post('atualiza-produto', 'ProdutosController@atualiza');
        Route::post('pedido', 'ProdutosController@pedido')->name('clube.pedido');

        Route::get('busca', 'ProdutosController@busca')->name('clube.busca');

        Route::get('maquinas', 'ClubeController@maquinas')->name('clube.maquinas');
        Route::get('maquinas/treinamento', 'ClubeController@treinamento')->name('clube.treinamento');
        Route::get('maquinas/manuais', 'ClubeController@manuais')->name('clube.manuais');
        Route::get('maquinas/orcamento', 'ClubeController@orcamentoMaquinas')->name('clube.orcamentoMaquinas');
        Route::post('maquinas/orcamento', 'ClubeController@orcamentoMaquinasPost')->name('clube.orcamentoMaquinasPost');
        Route::get('maquinas/videos/mais', 'ClubeController@videosMais')->name('clube.videosMais');
        Route::get('maquinas/videos/{video_slug?}', 'ClubeController@videos')->name('clube.videos');
    });
    Route::get('clube-treviolo/login', 'ClubeController@login')->name('clube.login');
    Route::post('clube-treviolo/login', 'ClubeController@loginPost')->name('clube.loginPost');
    Route::get('clube-treviolo/logout', 'ClubeController@logout')->name('clube.logout');

    Route::get('clube-treviolo/cadastro', 'ClubeController@cadastro')->name('clube.cadastro');
    Route::post('clube-treviolo/cadastro', 'ClubeController@cadastroPost')->name('clube.cadastroPost');

    Route::get('clube-treviolo/esqueci', 'ClubeController@esqueci')->name('clube.esqueci');
    Route::post('clube-treviolo/redefinir', 'Auth\PasswordController@sendResetLinkEmail')->name('clube.redefinir');
    Route::get('clube-treviolo/redefinicao/{token}', 'Auth\PasswordController@showResetForm');
    Route::post('clube-treviolo/reset', 'Auth\PasswordController@reset')->name('clube.reset');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth', 'authAdmin']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		// Route::resource('produtos/categorias', 'ProdutosCategoriasController', ['parameters' => ['categorias' => 'categorias_produtos']]);
		Route::resource('produtos', 'ProdutosController');
		Route::resource('videos', 'VideosController');
		Route::resource('treinamento', 'TreinamentoController', ['only' => ['index', 'update']]);
		Route::resource('manuais', 'ManuaisController');
		Route::resource('estabelecimentos', 'EstabelecimentosController');
		Route::resource('sobre', 'SobreController', ['only' => ['index', 'update']]);
		Route::resource('para-o-seu-negocio', 'ParaOSeuNegocioController', ['only' => ['index', 'update']]);
		Route::resource('nosso-blend', 'NossoBlendController', ['only' => ['index', 'update']]);
		Route::resource('processo-produtivo', 'ProcessoProdutivoController', ['only' => ['index', 'update']]);
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
		Route::resource('solucoes', 'SolucoesController');
		Route::resource('clientes', 'ClientesController');
		Route::resource('banners', 'BannersController');
		Route::resource('cabecalhos', 'CabecalhosController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');

        Route::get('orcamentos/produtos/{orcamentos_produtos}/toggle', ['as' => 'painel.orcamentos.produtos.toggle', 'uses' => 'OrcamentosController@toggle']);
        Route::resource('orcamentos/produtos', 'OrcamentosController', ['parameters' => ['produtos' => 'orcamentos_produtos']]);

        Route::get('orcamentos/maquinas/{maquinas}/toggle', ['as' => 'painel.orcamentos.maquinas.toggle', 'uses' => 'OrcamentosMaquinasController@toggle']);
        Route::resource('orcamentos/maquinas', 'OrcamentosMaquinasController');

        Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
        Route::resource('newsletter', 'NewsletterController');

        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
