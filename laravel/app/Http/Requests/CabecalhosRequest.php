<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CabecalhosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sobre' => 'image',
            'para_o_seu_negocio' => 'image',
            'processo_produtivo' => 'image',
            'nosso_blend' => 'image',
            'onde_encontrar' => 'image',
            'contato' => 'image',
        ];
    }
}
