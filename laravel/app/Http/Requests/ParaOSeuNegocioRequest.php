<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ParaOSeuNegocioRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'box_subtitulo' => 'required',
            'box_titulo' => 'required',
            'titulo_lateral' => 'required',
            'texto' => 'required',
            'titulo_grande' => 'required',
            'subtitulo_2' => 'required',
            'texto_2' => 'required',
            'subtitulo_3' => 'required',
            'texto_3' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
        ];
    }
}
