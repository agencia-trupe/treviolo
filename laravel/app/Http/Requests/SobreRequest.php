<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SobreRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'box_subtitulo' => 'required',
            'box_titulo' => 'required',
            'texto' => 'required',
            'titulo_inferior' => 'required',
            'subtitulo_2' => 'required',
            'titulo_2' => 'required',
            'texto_2' => 'required',
            'subtitulo_3' => 'required',
            'texto_3' => 'required',
            'subtitulo_4' => 'required',
            'texto_4' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
        ];
    }
}
