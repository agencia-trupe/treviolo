<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'empresa' => 'required',
            'depoimento' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
