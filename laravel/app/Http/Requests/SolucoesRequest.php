<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SolucoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'titulo' => 'required',
            'frase' => 'required',
            'link' => '',
            'novo' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
