<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'orientacao' => 'required',
            'imagem' => 'required|image',
            'subtitulo' => 'required',
            'titulo' => 'required',
            'link' => 'required_with:link_texto',
            'link_texto' => 'required_with:link',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
