<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'abertura_titulo' => 'required',
            'abertura_texto' => 'required',
            'abertura_link' => 'required',
            'frase_background' => 'required',
            'subtitulo_s' => 'required',
            'titulo_s' => 'required',
            'texto_s' => 'required',
        ];
    }
}
