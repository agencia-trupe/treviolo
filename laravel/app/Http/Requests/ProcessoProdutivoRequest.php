<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProcessoProdutivoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'box_subtitulo' => 'required',
            'box_titulo' => 'required',
            'titulo_lateral' => 'required',
            'texto' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
            'imagem_5' => 'image',
        ];
    }
}
