<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrcamentosMaquinasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'            => 'required',
            'sobrenome'       => 'required',
            'cpf'             => 'required|cpf',
            'telefone'        => 'required',
            'email'           => 'required|email',
            'cidade'          => 'required',
            'estabelecimento' => 'required',
            'pessoas'         => 'required',
            'consumo'         => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nome.required'            => 'Preencha seu nome.',
            'sobrenome.required'       => 'Preencha seu sobrenome.',
            'cpf.required'             => 'Preencha seu CPF.',
            'cpf.cpf'                  => 'Insira um CPF válido',
            'telefone.required'        => 'Preencha seu telefone.',
            'email.required'           => 'Preencha seu e-mail.',
            'email.email'              => 'Insira um endereço de e-mail válido.',
            'cidade.required'          => 'Preencha sua cidade.',
            'estabelecimento.required' => 'Preencha o tipo de estabelecimento.',
            'pessoas.required'         => 'Preencha quantas pessoas usam a máquina.',
            'consumo.required'         => 'Preencha o consumo mensal de café.',
        ];
    }
}
