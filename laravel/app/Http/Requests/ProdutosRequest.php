<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produtos_categoria_id' => 'required',
            'capa' => 'required|image',
            'titulo' => 'required',
            'subtitulo' => '',
            'descricao' => 'required',
            'quantidade_minima' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
