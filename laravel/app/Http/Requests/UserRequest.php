<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\User;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ];

        if ($this->get('acesso') === 'administrador') {
            $rules = array_merge($rules, [
                'nome' => 'required|max:255'
            ]);
        } else {
            $rules = array_merge($rules, [
                'cadastro' => 'required',
                'telefone' => 'required'
            ]);

            if ($this->get('cadastro') === 'pessoa_fisica') {
                $rules = array_merge($rules, [
                    'sobrenome' => 'required',
                    'cpf'       => 'required',
                ]);
            } else {
                $rules = array_merge($rules, [
                    'razao_social'       => 'required',
                    'nome_fantasia'      => 'required',
                    'cnpj'               => 'required',
                    'inscricao_estadual' => 'required',
                    'responsavel'        => 'required',
                ]);
            }
        }

        if ($this->method() != 'POST') {
            $rules['email'] = 'required|email|max:255|unique:users,email,'.$this->route('usuarios')->id;
            $rules['password'] = 'confirmed|min:6';
        }

        return $rules;
    }
}
