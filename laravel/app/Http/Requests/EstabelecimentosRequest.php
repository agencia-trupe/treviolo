<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EstabelecimentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'endereco' => 'required',
            'endereco_resumido' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function messages() {
        return [
            'lat.required' => 'Erro ao encontrar latitude do endereço. Selecione um endereço da lista do Google.',
            'lng.required' => 'Erro ao encontrar longitude do endereço. Selecione um endereço da lista do Google.'
        ];
    }
}
