<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VideosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'subtitulo' => '',
            'descricao' => 'required',
            'capa' => 'required|image',
            'video_tipo' => 'required',
            'video_codigo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
