<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('produtos', 'App\Models\Produto');
		$router->model('categorias_produtos', 'App\Models\ProdutoCategoria');
		$router->model('videos', 'App\Models\Video');
		$router->model('treinamento', 'App\Models\Treinamento');
		$router->model('manuais', 'App\Models\Manual');
		$router->model('estabelecimentos', 'App\Models\Estabelecimento');
		$router->model('sobre', 'App\Models\Sobre');
		$router->model('para-o-seu-negocio', 'App\Models\ParaOSeuNegocio');
		$router->model('nosso-blend', 'App\Models\NossoBlend');
		$router->model('processo-produtivo', 'App\Models\ProcessoProdutivo');
		$router->model('home', 'App\Models\Home');
		$router->model('solucoes', 'App\Models\Solucao');
		$router->model('clientes', 'App\Models\Cliente');
		$router->model('banners', 'App\Models\Banner');
		$router->model('cabecalhos', 'App\Models\Cabecalhos');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('orcamentos_produtos', 'App\Models\Orcamento');
        $router->model('maquinas', 'App\Models\OrcamentoMaquinas');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('video_slug', function($value) {
            return \App\Models\Video::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
