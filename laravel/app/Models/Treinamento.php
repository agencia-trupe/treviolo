<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Treinamento extends Model
{
    protected $table = 'treinamento';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 620,
            'height' => 415,
            'path'   => 'assets/img/treinamento/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 320,
            'height' => 525,
            'path'   => 'assets/img/treinamento/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 460,
            'height' => 230,
            'path'   => 'assets/img/treinamento/'
        ]);
    }

}
