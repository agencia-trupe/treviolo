<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ParaOSeuNegocio extends Model
{
    protected $table = 'para_o_seu_negocio';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 555,
            'height' => 820,
            'path'   => 'assets/img/para-o-seu-negocio/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 1125,
            'height' => 535,
            'path'   => 'assets/img/para-o-seu-negocio/'
        ]);
    }

}
