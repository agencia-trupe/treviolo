<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Sobre extends Model
{
    protected $table = 'sobre';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 1140,
            'height' => 725,
            'path'   => 'assets/img/sobre/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 1045,
            'height' => 495,
            'path'   => 'assets/img/sobre/'
        ]);
    }

}
