<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Estabelecimento extends Model
{
    protected $table = 'estabelecimentos';

    protected $guarded = ['id'];
}
