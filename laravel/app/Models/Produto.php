<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Produto extends Model
{
    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('produtos_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProdutoCategoria', 'produtos_categoria_id');
    }

    public function scopeBusca($query, $termo)
    {
        return $query
            ->where('titulo', 'LIKE', "%{$termo}%")
            ->orWhere('subtitulo', 'LIKE', "%{$termo}%");
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 220,
            'height' => 315,
            'path'   => 'assets/img/produtos/'
        ]);
    }

    public static function upload_miniatura()
    {
        return CropImage::make('miniatura', [
            'width'  => 60,
            'height' => 60,
            'path'   => 'assets/img/produtos/miniaturas/'
        ]);
    }
}
