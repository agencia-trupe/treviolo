<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cabecalhos extends Model
{
    protected $table = 'cabecalhos';

    protected $guarded = ['id'];

    public static function upload_sobre()
    {
        return CropImage::make('sobre', [
            'width'  => 2260,
            'height' => 620,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_para_o_seu_negocio()
    {
        return CropImage::make('para_o_seu_negocio', [
            'width'  => 2260,
            'height' => 620,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_processo_produtivo()
    {
        return CropImage::make('processo_produtivo', [
            'width'  => 2260,
            'height' => 620,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_nosso_blend()
    {
        return CropImage::make('nosso_blend', [
            'width'  => 2260,
            'height' => 620,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_onde_encontrar()
    {
        return CropImage::make('onde_encontrar', [
            'width'  => 2260,
            'height' => 620,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_contato()
    {
        return CropImage::make('contato', [
            'width'  => 2260,
            'height' => 620,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

}
