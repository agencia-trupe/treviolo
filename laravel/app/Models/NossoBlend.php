<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class NossoBlend extends Model
{
    protected $table = 'nosso_blend';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 355,
            'height' => 525,
            'path'   => 'assets/img/nosso-blend/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 605,
            'height' => 435,
            'path'   => 'assets/img/nosso-blend/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 565,
            'height' => 375,
            'path'   => 'assets/img/nosso-blend/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 285,
            'height' => 285,
            'path'   => 'assets/img/nosso-blend/'
        ]);
    }

    public static function upload_imagem_5()
    {
        return CropImage::make('imagem_5', [
            'width'  => 380,
            'height' => 250,
            'path'   => 'assets/img/nosso-blend/'
        ]);
    }

}
