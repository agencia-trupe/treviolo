export default function Banners() {
    $.fn.cycle.transitions.blockReveal = {
        before: function(opts, curr, next, fwd) {
            opts.animOut = { opacity: 0 };

            var current = $(curr);

            var orientacao = $(next).hasClass('slide-direita') ? 'rl' : 'lr';

            if ($(curr).find('h3').hasClass('block-revealer')) {
                $(curr).find('h3, h2, .link-wrapper, .imagem').removeClass('block-revealer').each(function(index, el) {
                    var content = $(el).find('.block-revealer__content').html();
                    $(el).find('.block-revealer__element, .block-revealer__content').remove();
                    $(el).html(content);
                });
            }

            var rev1 = new RevealFx(next.querySelector('h3'), {
                revealSettings : {
                    bgcolor: '#FBAE1A',
                    direction: orientacao,
                    delay: 150,
                    onCover: function(contentEl, revealerEl) {
                        contentEl.style.opacity = 1;
                    }
                }
            });
            var rev2 = new RevealFx(next.querySelector('h2'), {
                revealSettings : {
                    bgcolor: '#FBAE1A',
                    direction: orientacao,
                    delay: 400,
                    onCover: function(contentEl, revealerEl) {
                        contentEl.style.opacity = 1;
                    }
                }
            });
            if (next.querySelector('.link-wrapper')) {
                var rev3 = new RevealFx(next.querySelector('.link-wrapper'), {
                    revealSettings : {
                        bgcolor: '#FBAE1A',
                        direction: orientacao,
                        delay: 520,
                        onCover: function(contentEl, revealerEl) {
                            contentEl.style.opacity = 1;
                        }
                    }
                });
            }
            var rev4 = new RevealFx(next.querySelector('.imagem'), {
                revealSettings : {
                    bgcolor: '#000',
                    direction: orientacao,
                    delay: 520,
                    onCover: function(contentEl, revealerEl) {
                        contentEl.style.opacity = 1;
                    }
                }
            });

            $(next).css('visibility','visible');
            rev1.reveal();
            rev2.reveal();
            if (next.querySelector('.link-wrapper')) { rev3.reveal(); }
            rev4.reveal();
        }
    }

    $('.banners').cycle({
        slides: '> .slide',
        fx: 'blockReveal',
        pagerTemplate: '<a href=#>{{slideNum}}</a>',
    });
};
