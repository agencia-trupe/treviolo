export default function Animacoes() {
    //$.scrollSpeed(100, 800);

    var isScrolledIntoView = function(el) {
        var elemTop = $(el)[0].getBoundingClientRect().top;
        var elemBottom = $(el)[0].getBoundingClientRect().bottom;
        return (elemTop < window.innerHeight && elemBottom >= 0);
    }

    $(window).on('scroll', function() {
        if (isScrolledIntoView('footer')) {
            $('footer').addClass('visible');
        } else {
            $('footer').removeClass('visible');
        }

        if ($('.letra').length) {
            if (isScrolledIntoView('.letra')) {
                $('.letra').addClass('visible');
            } else {
                $('.letra').removeClass('visible');
            }
        }
    });

    $('body').on('mouseenter mouseleave', '.solucao', function() {
        $(this).find('.texto').slideToggle({duration: 250});
    });
};
