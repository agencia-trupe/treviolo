export default function Produtos() {
    $('.contador').on('input', function(event) {
        this.value = this.value.replace(/\D/g, '');
    });

    $('.contador').on('blur', function() {
        var minimo = $(this).parent().data('minimo');

        if (this.value < minimo) {
            this.value = minimo;
        }
    });

    $('.menos').click(function(event) {
        event.preventDefault();

        var contador = $(this).siblings('.contador'),
            quantidade = contador.val(),
            minimo = $(this).parent().data('minimo');

        quantidade = quantidade - 1;
        if (quantidade < minimo) return;

        contador.val(quantidade);
    });

    $('.mais').click(function(event) {
        event.preventDefault();

        var contador = $(this).siblings('.contador'),
            quantidade = contador.val();

        contador.val(parseInt(quantidade) + 1);
    });

    $('.btn-adicionar').click(function(event) {
        event.preventDefault();

        var _this = $(this);

        if (_this.hasClass('sending')) return false;

        _this.addClass('sending');

        var produtoId  = _this.prev().data('id'),
            quantidade = _this.prev().find('.contador').val();

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/clube-treviolo/adiciona-produto',
            data: {
                id: produtoId,
                quantidade: parseInt(quantidade),
            },
            success: function(data) {
                $.growl.notice({ title: 'Pedido', message: data.message, size: 'large' });
            },
            error: function(data) {
                var message = data.responseJSON || 'Erro interno do servidor.';
                $.growl.error({ title: 'Pedido', message: message, size: 'large' });
            },
            dataType: 'json'
        })
        .always(function() {
            _this.removeClass('sending');
        });
    });

    $('.btn-excluir').click(function(event) {
        event.preventDefault();

        var _this = $(this);

        if (_this.hasClass('sending')) return false;

        _this.addClass('sending');

        var produtoId = _this.parent().data('id');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/clube-treviolo/exclui-produto',
            data: {
                id: produtoId
            },
            success: function(data) {
                _this.parent().fadeOut(function() {
                    $(this).remove();
                    if (!$('.lista-checkout .produto').length) {
                        $('.lista-checkout .header, .form-pedido').fadeOut();
                        $('.mensagem-cliente-js').fadeIn();
                    }
                });
                $.growl.notice({ title: 'Pedido', message: data.message, size: 'large' });
            },
            error: function(data) {
                var message = data.responseJSON || 'Erro interno do servidor.';
                $.growl.error({ title: 'Pedido', message: message, size: 'large' });
            },
            dataType: 'json'
        })
        .always(function() {
            _this.removeClass('sending');
        });
    });

    $('.btn-atualizar').click(function(event) {
        event.preventDefault();

        var _this = $(this);

        if (_this.hasClass('sending')) return false;

        _this.addClass('sending');

        var produtoId = _this.parent().parent().data('id'),
            quantidade = _this.prev().find('.contador').val();

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/clube-treviolo/atualiza-produto',
            data: {
                id: produtoId,
                quantidade: quantidade
            },
            success: function(data) {
                $.growl.notice({ title: 'Pedido', message: data.message, size: 'large' });
            },
            error: function(data) {
                var message = data.responseJSON || 'Erro interno do servidor.';
                $.growl.error({ title: 'Pedido', message: message, size: 'large' });
            },
            dataType: 'json'
        });
    });
};
