export default function FormularioCadastro() {
    $('#cpf').inputmask('999.999.999-99', { jitMasking: true });
    $('#cnpj').inputmask('99.999.999/9999-99', { jitMasking: true });
    $('#telefone').inputmask({ mask: ['(99) 9999-9999', '(99) 99999-9999'], jitMasking: true });

    if (!$('input[name=cadastro]').length) return;

    if ($('input[name=cadastro]:checked').val() === 'pessoa_fisica') {
        $('.cadastro-pj').hide();
    } else {
        $('.cadastro-pf').hide();
    }

    $('input[name=cadastro]').change(function() {
        if ($(this).val() === 'pessoa_fisica') {
            $('.cadastro-pj').hide();
            $('.cadastro-pf').show();
        } else {
            $('.cadastro-pf').hide();
            $('.cadastro-pj').show();
        }
    });
};
