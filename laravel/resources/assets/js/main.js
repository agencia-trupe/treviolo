import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Banners from './Banners';
import Animacoes from './Animacoes';
import FormularioNewsletter from './FormularioNewsletter';
import FormularioContato from './FormularioContato';
import MascaraCep from './MascaraCep';
import FormularioCadastro from './FormularioCadastro';
import Produtos from './Produtos';

AjaxSetup();
MobileToggle();
Banners();
Animacoes();
FormularioNewsletter();
FormularioContato();
MascaraCep();
FormularioCadastro();
Produtos();
