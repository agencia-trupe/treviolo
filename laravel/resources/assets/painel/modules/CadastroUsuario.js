export default function CadastroUsuario() {
    if (!$('.cadastro-user-painel').length) return;

    $('.cadastro-admin, .cadastro-pj, .cadastro-pf').hide();

    if ($('select[name=acesso]').val() === 'administrador') {
        $('.cadastro-admin').show();
    } else {
        if ($('select[name=cadastro]').val() === 'pessoa_fisica') {
            $('.cadastro-pf').show();
        } else {
            $('.cadastro-pj').show();
        }
    }

    $('select[name=acesso]').change(function() {
        if ($(this).val() === 'administrador') {
            $('.cadastro-pf, .cadastro-pj').hide();
            $('.cadastro-admin').show();
        } else {
            $('.cadastro-admin').hide();

            if ($('select[name=cadastro]').val() === 'pessoa_fisica') {
                $('.cadastro-pf').show();
            } else {
                $('.cadastro-pj').show();
            }
        }
    });

    $('select[name=cadastro]').change(function() {
        if ($('select[name=cadastro]').val() === 'pessoa_fisica') {
            $('.cadastro-pj').hide();
            $('.cadastro-pf').show();
        } else {
            $('.cadastro-pf').hide();
            $('.cadastro-pj').show();
        }
    });
};
