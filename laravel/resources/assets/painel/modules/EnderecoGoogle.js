export default function EnderecoGoogle() {

    var autocomplete;

    if (!$('#placesAutoComplete').length) return;

    autocomplete = new google.maps.places.Autocomplete(
        document.getElementById('placesAutoComplete'),
        {
            types: ['address'],
            componentRestrictions: { country: 'br' }
        }
    );

    autocomplete.addListener('place_changed', fillCoordinates);

    function fillCoordinates() {
        var place = autocomplete.getPlace();

        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();

        $('#lat').val(lat);
        $('#lng').val(lng);

        $('input[type="submit"]').prop('disabled', false);
    }

    $('#placesAutoComplete').keydown(function() {
        $('#lat').val('');
        $('#lng').val('');

        $('input[type="submit"]').prop('disabled', true);
    });
};
