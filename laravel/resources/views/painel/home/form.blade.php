@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('abertura_titulo', 'Abertura Título') !!}
    {!! Form::textarea('abertura_titulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'titulo']) !!}
</div>

<div class="form-group">
    {!! Form::label('abertura_texto', 'Abertura Texto') !!}
    {!! Form::textarea('abertura_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('abertura_link', 'Abertura Link') !!}
    {!! Form::text('abertura_link', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('frase_background', 'Frase Background') !!}
    {!! Form::textarea('frase_background', null, ['class' => 'form-control ckeditor', 'data-editor' => 'titulo']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('subtitulo_s', 'Subtítulo S') !!}
    {!! Form::text('subtitulo_s', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_s', 'Título S') !!}
    {!! Form::textarea('titulo_s', null, ['class' => 'form-control ckeditor', 'data-editor' => 'titulo']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_s', 'Texto S') !!}
    {!! Form::textarea('texto_s', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
