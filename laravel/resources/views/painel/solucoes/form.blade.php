@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (305x460px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/solucoes/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::textarea('frase', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <div class="checkbox">
        <label>
            <input type="hidden" name="novo" value="0">
            {!! Form::checkbox('novo', 1) !!} Novo
        </label>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.solucoes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
