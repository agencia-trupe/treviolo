@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('box_subtitulo', 'Box Subtítulo') !!}
    {!! Form::text('box_subtitulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('box_titulo', 'Box Título') !!}
    {!! Form::textarea('box_titulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'titulo']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo_lateral', 'Título Lateral') !!}
    {!! Form::textarea('titulo_lateral', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1 (355x525px)') !!}
            <img src="{{ url('assets/img/nosso-blend/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2 (605x435px)') !!}
            <img src="{{ url('assets/img/nosso-blend/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3 (565x375px)') !!}
            <img src="{{ url('assets/img/nosso-blend/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4 (285x285px)') !!}
            <img src="{{ url('assets/img/nosso-blend/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_5', 'Imagem 5 (380x250px)') !!}
            <img src="{{ url('assets/img/nosso-blend/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
