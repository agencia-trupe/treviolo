@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Nosso Blend</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.nosso-blend.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.nosso-blend.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
