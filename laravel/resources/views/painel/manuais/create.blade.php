@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Manuais /</small> Adicionar Manual</h2>
    </legend>

    {!! Form::open(['route' => 'painel.manuais.store', 'files' => true]) !!}

        @include('painel.manuais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
