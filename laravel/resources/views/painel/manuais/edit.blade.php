@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Manuais /</small> Editar Manual</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.manuais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.manuais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
