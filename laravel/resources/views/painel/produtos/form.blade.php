@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produtos_categoria_id', 'Categoria') !!}
    {!! Form::select('produtos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa (220x315px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('miniatura', 'Miniatura (60x60px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/miniaturas/'.$registro->miniatura) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('miniatura', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo', 'Subtítulo') !!}
    {!! Form::text('subtitulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'titulo']) !!}
</div>

<div class="form-group">
    {!! Form::label('quantidade_minima', 'Quantidade mínima') !!}
    {!! Form::number('quantidade_minima', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
