@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('empresa', 'Empresa') !!}
    {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('depoimento', 'Depoimento') !!}
    {!! Form::textarea('depoimento', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.clientes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
