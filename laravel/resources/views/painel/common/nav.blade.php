<ul class="nav navbar-nav">
    <li @if(str_is('painel.cabecalhos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.cabecalhos.index') }}">Cabeçalhos</a>
    </li>
    <li class="dropdown @if(str_is('painel.home*', Route::currentRouteName()) || str_is('painel.banners*', Route::currentRouteName()) || str_is('painel.solucoes*', Route::currentRouteName()) || str_is('painel.clientes*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Textos</a>
            </li>
            <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(str_is('painel.solucoes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.solucoes.index') }}">Soluções</a>
            </li>
            <li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.clientes.index') }}">Clientes</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.sobre*', Route::currentRouteName()) || str_is('painel.para-o-seu-negocio*', Route::currentRouteName()) || str_is('painel.processo-produtivo*', Route::currentRouteName()) || str_is('painel.nosso-blend*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Páginas
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.sobre*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.sobre.index') }}">Sobre</a>
            </li>
        	<li @if(str_is('painel.para-o-seu-negocio*', Route::currentRouteName())) class="active" @endif>
        		<a href="{{ route('painel.para-o-seu-negocio.index') }}">Para o seu negócio</a>
        	</li>
            <li @if(str_is('painel.processo-produtivo*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.processo-produtivo.index') }}">Processo Produtivo</a>
            </li>
            <li @if(str_is('painel.nosso-blend*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.nosso-blend.index') }}">Nosso Blend</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.estabelecimentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.estabelecimentos.index') }}">Estabelecimentos</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.produtos*', Route::currentRouteName()) || str_is('painel.manuais*', Route::currentRouteName()) || str_is('painel.treinamento*', Route::currentRouteName()) || str_is('painel.videos*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Clube Treviolo
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.produtos.index') }}">Produtos</a>
            </li>
            <li @if(str_is('painel.manuais*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.manuais.index') }}">Manuais</a>
            </li>
            <li @if(str_is('painel.treinamento*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.treinamento.index') }}">Treinamento</a>
            </li>
            <li @if(str_is('painel.videos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.videos.index') }}">Vídeos</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.orcamentos*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Orçamentos
            @if($maquinasNaoLidos >= 1 || $produtosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $maquinasNaoLidos + $produtosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.orcamentos.produtos.index') }}">
                Produtos
                @if($produtosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $produtosNaoLidos }}</span>
                @endif
            </a></li>
            <li><a href="{{ route('painel.orcamentos.maquinas.index') }}">
                Máquinas
                @if($maquinasNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $maquinasNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
    </li>
</ul>
