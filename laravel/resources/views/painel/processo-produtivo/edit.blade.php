@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Processo Produtivo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.processo-produtivo.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.processo-produtivo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
