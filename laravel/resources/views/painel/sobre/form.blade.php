@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('box_subtitulo', 'Box Subtítulo') !!}
    {!! Form::text('box_subtitulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('box_titulo', 'Box Título') !!}
    {!! Form::textarea('box_titulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'titulo']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_inferior', 'Título Inferior') !!}
    {!! Form::textarea('titulo_inferior', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('subtitulo_2', 'Subtítulo 2') !!}
    {!! Form::text('subtitulo_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_2', 'Título 2') !!}
    {!! Form::textarea('titulo_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'titulo']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_2', 'Texto 2') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textoCompleto']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('subtitulo_3', 'Subtítulo 3') !!}
    {!! Form::text('subtitulo_3', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_3', 'Texto 3') !!}
    {!! Form::textarea('texto_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textoCompleto']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('subtitulo_4', 'Subtítulo 4') !!}
    {!! Form::text('subtitulo_4', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_4', 'Texto 4') !!}
    {!! Form::textarea('texto_4', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textoCompleto']) !!}
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem 1 (1140x725px)') !!}
    <img src="{{ url('assets/img/sobre/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_2', 'Imagem 2 (1045x495px)') !!}
    <img src="{{ url('assets/img/sobre/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
