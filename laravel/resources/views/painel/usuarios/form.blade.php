@include('painel.common.flash')

<div class="cadastro-user-painel">
    <div class="form-group">
        {!! Form::label('acesso', 'Acesso') !!}
        {!! Form::select('acesso', ['cliente' => 'Cliente', 'administrador' => 'Administrador'], null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-pf cadastro-pj">
        {!! Form::label('cadastro', 'Cadastro') !!}
        {!! Form::select('cadastro', ['pessoa_fisica' => 'Pessoa Física', 'pessoa_juridica' => 'Pessoa Jurídica'], null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-admin cadastro-pf">
        {!! Form::label('nome', 'Nome') !!}
        {!! Form::text('nome', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-pf">
        {!! Form::label('sobrenome', 'Sobrenome') !!}
        {!! Form::text('sobrenome', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-pf">
        {!! Form::label('cpf', 'CPF') !!}
        {!! Form::text('cpf', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-pj">
        {!! Form::label('razao_social', 'Razão Social') !!}
        {!! Form::text('razao_social', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-pj">
        {!! Form::label('nome_fantasia', 'Nome Fantasia') !!}
        {!! Form::text('nome_fantasia', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-pj">
        {!! Form::label('cnpj', 'CNPJ') !!}
        {!! Form::text('cnpj', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-pj">
        {!! Form::label('inscricao_estadual', 'Inscrição Estadual') !!}
        {!! Form::text('inscricao_estadual', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-pj">
        {!! Form::label('responsavel', 'Responsável') !!}
        {!! Form::text('responsavel', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group cadastro-pf cadastro-pj">
        {!! Form::label('telefone', 'Telefone') !!}
        {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'E-mail') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password', 'Senha') !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password_confirmation', 'Confirmação de Senha') !!}
        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.usuarios.index') }}" class="btn btn-default btn-voltar">Voltar</a>
