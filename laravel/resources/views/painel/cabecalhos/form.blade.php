@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('sobre', 'Sobre (2260x620px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->sobre) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('sobre', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('para_o_seu_negocio', 'Para o seu negócio (2260x620px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->para_o_seu_negocio) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('para_o_seu_negocio', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('processo_produtivo', 'Processo produtivo (2260x620px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->processo_produtivo) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('processo_produtivo', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('nosso_blend', 'Nosso blend (2260x620px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->nosso_blend) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('nosso_blend', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('onde_encontrar', 'Onde encontrar (2260x620px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->onde_encontrar) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('onde_encontrar', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('contato', 'Contato (2260x620px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->contato) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('contato', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
