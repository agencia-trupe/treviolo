@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Cabeçalhos</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cabecalhos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cabecalhos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
