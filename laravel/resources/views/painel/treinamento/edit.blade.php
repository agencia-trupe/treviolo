@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Treinamento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.treinamento.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.treinamento.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
