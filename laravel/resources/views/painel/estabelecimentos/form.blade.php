@include('painel.common.flash')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmja-YGrGPSUliYIezCECDq9NC7bGhOWY&libraries=places"></script>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço (selecione um endereço da lista do Google)') !!}
    {!! Form::text('endereco', null, ['class' => 'form-control', 'id' => 'placesAutoComplete']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco_resumido', 'Endereço Resumido (apenas para visualização na lista de estebelecimentos)') !!}
    {!! Form::text('endereco_resumido', null, ['class' => 'form-control']) !!}
</div>

{!! Form::hidden('lat', null, ['class' => 'form-control', 'id' => 'lat']) !!}
{!! Form::hidden('lng', null, ['class' => 'form-control', 'id' => 'lng']) !!}

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.estabelecimentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
