@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Estabelecimentos /</small> Editar Estabelecimento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.estabelecimentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.estabelecimentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
