@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Orçamentos de máquinas</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

    <div class="form-group">
        <label>Sobrenome</label>
        <div class="well">{{ $contato->sobrenome }}</div>
    </div>

    <div class="form-group">
        <label>CPF</label>
        <div class="well">{{ $contato->cpf }}</div>
    </div>

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $contato->telefone }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $contato->email }}
        </div>
    </div>

    <div class="form-group">
        <label>Cidade</label>
        <div class="well">{{ $contato->cidade }}</div>
    </div>

    <div class="form-group">
        <label>Tipo de estabelecimento</label>
        <div class="well">{{ $contato->estabelecimento }}</div>
    </div>

    <div class="form-group">
        <label>Quantas pessoas usam a máquina</label>
        <div class="well">{{ $contato->pessoas }}</div>
    </div>

    <div class="form-group">
        <label>Consumo de café por mês</label>
        <div class="well">{{ $contato->consumo }}</div>
    </div>

    <a href="{{ route('painel.orcamentos.maquinas.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
