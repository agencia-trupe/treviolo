@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Orçamentos de produtos</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

@if($contato->cliente->cadastro === 'pessoa_fisica')
    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->cliente ? $contato->cliente->nome : '' }}</div>
    </div>

    <div class="form-group">
        <label>Sobrenome</label>
        <div class="well">{{ $contato->cliente ? $contato->cliente->sobrenome : '' }}</div>
    </div>

    <div class="form-group">
        <label>CPF</label>
        <div class="well">{{ $contato->cliente ? $contato->cliente->cpf : '' }}</div>
    </div>
@else
    <div class="form-group">
        <label>Razão Social</label>
        <div class="well">{{ $contato->cliente ? $contato->cliente->razao_social : '' }}</div>
    </div>

    <div class="form-group">
        <label>Nome Fantasia</label>
        <div class="well">{{ $contato->cliente ? $contato->cliente->nome_fantasia : '' }}</div>
    </div>

    <div class="form-group">
        <label>CNPJ</label>
        <div class="well">{{ $contato->cliente ? $contato->cliente->cnpj : '' }}</div>
    </div>

    <div class="form-group">
        <label>Inscrição Estadual</label>
        <div class="well">{{ $contato->cliente ? $contato->cliente->inscricao_estadual : '' }}</div>
    </div>

    <div class="form-group">
        <label>Responsável</label>
        <div class="well">{{ $contato->cliente ? $contato->cliente->responsavel : '' }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $contato->cliente ? $contato->cliente->telefone : '' }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->cliente ? $contato->cliente->email : '' }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $contato->cliente ? $contato->cliente->email : '' }}
        </div>
    </div>

    <div class="form-group">
        <label>Orçamento</label>
        <div class="well">{!! $contato->orcamento !!}</div>
    </div>

    <a href="{{ route('painel.orcamentos.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
