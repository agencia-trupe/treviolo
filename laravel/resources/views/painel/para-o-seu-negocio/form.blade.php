@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('box_subtitulo', 'Box Subtítulo') !!}
    {!! Form::text('box_subtitulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('box_titulo', 'Box Título') !!}
    {!! Form::textarea('box_titulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'titulo']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo_lateral', 'Título Lateral') !!}
    {!! Form::textarea('titulo_lateral', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo_grande', 'Título Grande') !!}
    {!! Form::text('titulo_grande', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('subtitulo_2', 'Subtítulo 2') !!}
    {!! Form::text('subtitulo_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_2', 'Texto 2') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textoCompleto']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('subtitulo_3', 'Subtítulo 3') !!}
    {!! Form::text('subtitulo_3', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_3', 'Texto 3') !!}
    {!! Form::textarea('texto_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textoCompleto']) !!}
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem 1 (555x820px)') !!}
    <img src="{{ url('assets/img/para-o-seu-negocio/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_2', 'Imagem 2 (1125x535px)') !!}
    <img src="{{ url('assets/img/para-o-seu-negocio/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
