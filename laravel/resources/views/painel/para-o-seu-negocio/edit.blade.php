@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Para o seu negócio</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.para-o-seu-negocio.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.para-o-seu-negocio.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
