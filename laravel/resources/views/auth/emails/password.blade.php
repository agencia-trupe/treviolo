<!DOCTYPE html>
<html>
<head>
    <title>[REDEFINIÇÃO DE SENHA] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='color:#000;font-size:14px;font-family:Verdana;'>
        <a href="{{ url('clube-treviolo/redefinicao', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">Clique aqui para redefinir sua senha.</a>
    </span>
</body>
</html>
