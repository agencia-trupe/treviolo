@extends('frontend.common.template')

@section('content')

    <div class="main onde-encontrar">
        <div class="cabecalho" style="background-image:url('{{ asset('assets/img/cabecalhos/'.$cabecalhos->onde_encontrar) }}')"></div>

        <div class="center">
            <div class="box box-right">
                <div class="texto">
                    <h3>ONDE ENCONTRAR</h3>
                    <h2>
                        Saiba onde tomar<br>
                        o café Treviolo mais<br>
                        próximo de você
                    </h2>
                </div>
            </div>

            <h2 class="frase">Estamos nas melhores casas de São Paulo</h2>
        </div>

        <div class="mapa-wrapper">
            <div class="center">
                <div class="mapa">
                    <div id="map"></div>
                        <script>
                            function initMap() {
                                var map = new google.maps.Map(document.getElementById('map'), {
                                    zoom: 4,
                                    mapTypeControl: false,
                                    streetViewControl: false,
                                    styles: [
                                      {
                                        "elementType": "geometry",
                                        "stylers": [
                                          {
                                            "color": "#212121"
                                          }
                                        ]
                                      },
                                      {
                                        "elementType": "labels.icon",
                                        "stylers": [
                                          {
                                            "visibility": "off"
                                          }
                                        ]
                                      },
                                      {
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                          {
                                            "color": "#757575"
                                          }
                                        ]
                                      },
                                      {
                                        "elementType": "labels.text.stroke",
                                        "stylers": [
                                          {
                                            "color": "#212121"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "administrative",
                                        "elementType": "geometry",
                                        "stylers": [
                                          {
                                            "color": "#757575"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "administrative.country",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                          {
                                            "color": "#9e9e9e"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "administrative.land_parcel",
                                        "stylers": [
                                          {
                                            "visibility": "off"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "administrative.locality",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                          {
                                            "color": "#bdbdbd"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "poi",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                          {
                                            "color": "#757575"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "poi.park",
                                        "elementType": "geometry",
                                        "stylers": [
                                          {
                                            "color": "#181818"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "poi.park",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                          {
                                            "color": "#616161"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "poi.park",
                                        "elementType": "labels.text.stroke",
                                        "stylers": [
                                          {
                                            "color": "#1b1b1b"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "road",
                                        "elementType": "geometry.fill",
                                        "stylers": [
                                          {
                                            "color": "#2c2c2c"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "road",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                          {
                                            "color": "#8a8a8a"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "road.arterial",
                                        "elementType": "geometry",
                                        "stylers": [
                                          {
                                            "color": "#373737"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "road.highway",
                                        "elementType": "geometry",
                                        "stylers": [
                                          {
                                            "color": "#3c3c3c"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "road.highway.controlled_access",
                                        "elementType": "geometry",
                                        "stylers": [
                                          {
                                            "color": "#4e4e4e"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "road.local",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                          {
                                            "color": "#616161"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "transit",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                          {
                                            "color": "#757575"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "water",
                                        "elementType": "geometry",
                                        "stylers": [
                                          {
                                            "color": "#000000"
                                          }
                                        ]
                                      },
                                      {
                                        "featureType": "water",
                                        "elementType": "labels.text.fill",
                                        "stylers": [
                                          {
                                            "color": "#3d3d3d"
                                          }
                                        ]
                                      }
                                    ]
                                });

                                var bounds = new google.maps.LatLngBounds();

                                window.markers = [
                                    @foreach($estabelecimentos as $estabelecimento)
                                        {
                                            coords: {
                                                lat: {{ $estabelecimento->lat }},
                                                lng: {{ $estabelecimento->lng }}
                                            },
                                            text: "{!! '<strong>'.$estabelecimento->nome.'</strong><br>'.$estabelecimento->endereco_resumido !!}"
                                        },
                                    @endforeach
                                ];

                                window.markersArray = [];
                                var infoWindowArray = [];
                                for (i in markers) {
                                    var markerPosition = new google.maps.LatLng(markers[i].coords.lat, markers[i].coords.lng);
                                    markersArray.push(new google.maps.Marker({
                                            position: markerPosition,
                                            map: map,
                                    }));

                                    bounds.extend(markerPosition);

                                    infoWindowArray.push(new google.maps.InfoWindow({
                                        content: markers[i].text,
                                        maxWidth: 250
                                    }));

                                    if (markers[i].text != '') {
                                        google.maps.event.addListener(markersArray[i], 'click', (function(i) {
                                            return function() {
                                                infoWindowArray.map(function(info) {
                                                    info.close();
                                                });
                                                infoWindowArray[i].open(map, markersArray[i]);
                                                map.setZoom(16);
                                                map.setCenter(markersArray[i].getPosition());
                                            }
                                        })(i));
                                    }
                                }

                                map.fitBounds(bounds);
                            }
                        </script>
                        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmja-YGrGPSUliYIezCECDq9NC7bGhOWY&callback=initMap"></script>
                </div>

                @if(session('erro'))
                <div class="erro">{{ session('erro') }}</div>
                @endif

                <div class="lista-estabelecimentos">
                    <form action="{{ route('onde-encontrar') }}" method="GET">
                        <input type="text" name="cep" id="cep" placeholder="DIGITE O SEU CEP" value="{{ request('cep') }}">
                        <input type="submit" value="OK">
                    </form>

                    <div class="estabelecimentos">
                        <?php $i = 0; ?>
                        @foreach($estabelecimentos as $estabelecimento)
                        <a href="javascript:google.maps.event.trigger(markersArray[{{ $i++ }}],'click');">
                            <strong>{{ $estabelecimento->nome }}</strong><br>
                            {{ $estabelecimento->endereco_resumido }}<br>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
