@extends('frontend.common.template')

@section('content')

    <div class="main sobre">
        <div class="cabecalho" style="background-image:url('{{ asset('assets/img/cabecalhos/'.$cabecalhos->sobre) }}')"></div>

        <div class="center">
            <div class="box box-right">
                <div class="texto">
                    <h3>{{ $sobre->box_subtitulo }}</h3>
                    <h2>
                        {!! $sobre->box_titulo !!}
                    </h2>
                </div>
            </div>

            <div class="texto-wrapper">
                <div class="graos"></div>

                <div class="texto">
                    {!! $sobre->texto !!}

                    <p class="titulo-inferior">{!! $sobre->titulo_inferior !!}</p>
                </div>
            </div>

            <div class="texto2-wrapper">
                <div class="left">
                    <div class="box-2">
                        <div class="texto">
                            <h3>{{ $sobre->subtitulo_2 }}</h3>
                            <h2>{!! $sobre->titulo_2 !!}</h2>
                        </div>
                    </div>

                    <div class="texto2">
                        {!! $sobre->texto_2 !!}
                    </div>
                </div>

                <div class="imagem">
                    <img src="{{ asset('assets/img/sobre/'.$sobre->imagem_1) }}" alt="">
                </div>
            </div>

            <div class="texto3-wrapper">
                <div class="texto">
                    <h3>{{ $sobre->subtitulo_3 }}</h3>
                    {!! $sobre->texto_3 !!}
                </div>
            </div>

            <div class="texto4-wrapper">
                <div class="box-3">
                    <div class="texto">
                        <h3>{{ $sobre->subtitulo_4 }}</h3>
                        {!! $sobre->texto_4 !!}
                        <a href="{{ route('contato') }}">ESCREVA</a>
                    </div>
                </div>

                <div class="imagem">
                    <img src="{{ asset('assets/img/sobre/'.$sobre->imagem_2) }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection
