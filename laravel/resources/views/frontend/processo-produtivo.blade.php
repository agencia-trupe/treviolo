@extends('frontend.common.template')

@section('content')

    <div class="main processo-produtivo">
        <div class="cabecalho" style="background-image:url('{{ asset('assets/img/cabecalhos/'.$cabecalhos->processo_produtivo) }}')"></div>

        <div class="center">
            <div class="box box-right">
                <div class="texto">
                    <h3>{{ $processoProdutivo->box_subtitulo }}</h3>
                    <h2>
                        {!! $processoProdutivo->box_titulo !!}
                    </h2>
                </div>
            </div>

            <div class="texto-wrapper">
                <div class="titulo">
                    <h2>{!! $processoProdutivo->titulo_lateral !!}</h2>
                </div>
                <div class="texto">
                    {!! $processoProdutivo->texto !!}
                </div>
            </div>

            <div class="grupo-imagens">
                <div class="top">
                    <img src="{{ asset('assets/img/processo-produtivo/'.$processoProdutivo->imagem_1) }}" alt="">
                    <img src="{{ asset('assets/img/processo-produtivo/'.$processoProdutivo->imagem_2) }}" alt="">
                </div>
                <div class="left">
                    <img src="{{ asset('assets/img/processo-produtivo/'.$processoProdutivo->imagem_3) }}" alt="">
                </div>
                <div class="right">
                    <img src="{{ asset('assets/img/processo-produtivo/'.$processoProdutivo->imagem_4) }}" alt="">
                    <img src="{{ asset('assets/img/processo-produtivo/'.$processoProdutivo->imagem_5) }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection
