@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="center" style="background:#FFF">
            <div class="clube-container clube-videos">
                <a href="{{ route('clube.maquinas') }}" class="btn-voltar">VOLTAR</a>
                <a href="{{ route('clube.checkout') }}" class="btn-sacola"><span>SACOLA DE COMPRAS</span></a>

                <div class="titulo">
                    <h1>Vídeos tutoriais</h1>
                    <p>Precisa de ajuda ou está com dúvidas sobre como usar sua máquina?<br>Veja nossos vídeos e confira o passo a passo.</p>
                </div>

                <div class="video-aberto">
                    <div class="embed-video">
                        <iframe src="{{ $video->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$video->video_codigo : 'https://player.vimeo.com/video/'.$video->video_codigo }}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
                    </div>
                    <div class="texto">
                        <h2>
                            {{ $video->titulo }}
                            @if($video->subtitulo)
                            <span>&middot;</span>
                            {{ $video->subtitulo }}
                            @endif
                        </h2>
                        <p>{!! $video->descricao !!}</p>
                    </div>
                </div>

                <div class="ultimos-videos">
                    <h3>ÚLTIMOS VÍDEOS</h3>
                    <div class="videos-thumbs">
                        @foreach($ultimos as $v)
                        <a href="{{ route('clube.videos', $v->slug) }}">
                            <div class="imagem">
                                <img src="{{ asset('assets/img/videos/'.$v->capa) }}" alt="">
                                <div class="play"></div>
                            </div>
                            <div class="texto">
                                <h4>
                                    {{ $v->titulo }}
                                    @if($v->subtitulo)
                                    <span>&middot;</span>
                                    {{ $v->subtitulo }}
                                    @endif
                                </h4>
                                <p>{!! $v->descricao !!}</p>
                            </div>
                        </a>
                        @endforeach
                    </div>
                    <a href="{{ route('clube.videosMais') }}" class="btn-mais">MAIS</a>
                </div>
            </div>
        </div>
    </div>

@endsection
