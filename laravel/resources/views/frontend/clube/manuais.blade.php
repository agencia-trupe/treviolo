@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="center" style="background:#FFF">
            <div class="clube-container clube-manuais">
                <a href="{{ route('clube.maquinas') }}" class="btn-voltar">VOLTAR</a>
                <a href="{{ route('clube.checkout') }}" class="btn-sacola"><span>SACOLA DE COMPRAS</span></a>

                <div class="titulo">
                    <h1>Manuais técnicos</h1>
                    <p>Quer conhecer mais ou tirar dúvidas sobre a sua máquina?<br>Baixe aqui o manual técnico dela.</p>
                </div>

                <div class="lista-manuais">
                    <div class="header">
                        <span>Lista</span>
                        <span>Baixar</span>
                    </div>
                    @foreach($manuais as $manual)
                    <a href="{{ url('assets/manuais/'.$manual->arquivo) }}" target="_blank">
                        <span>{{ $manual->titulo }}</span>
                        <span>Baixar</span>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
