@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="center" style="background:#FFF">
            <div class="clube-container clube-esqueci">
                <a href="{{ route('clube.login') }}" class="btn-voltar">VOLTAR</a>

                <div class="titulo">
                    <h1>Esqueceu a sua senha?</h1>
                    <p>Você veio ao lugar certo para redefinir uma senha que esqueceu.<br>Enviaremos um e-mail com as instruções para criar uma nova senha.</p>
                </div>

                @if(session('senhaRedefinida'))
                <div class="senha-redefinida">Instruções para redefinição de sua senha foram enviadas para o seu e-mail.</div>
                @else
                <form action="{{ route('clube.redefinir') }}" method="POST">
                    {!! csrf_field() !!}
                    @if($errors->any())
                        <div class="erros">
                            @foreach($errors->all() as $error)
                            <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif
                    <input type="email" name="email" placeholder="E-MAIL" value="{{ old('email') }}" required>
                    <input type="submit" value="ENVIAR">
                </form>
                @endif
            </div>
        </div>
    </div>

@endsection
