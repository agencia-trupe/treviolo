@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="center" style="background:#FFF">
            <div class="clube-index">
                <a href="{{ route('clube.graos') }}">
                    <div class="titulo">
                        <span>Grãos<br>Treviolo</span>
                    </div>
                    <img src="{{ asset('assets/img/layout/clube-graos.jpg') }}" alt="">
                </a>
                <a href="{{ route('clube.maquinas') }}">
                    <img src="{{ asset('assets/img/layout/clube-maquinas.jpg') }}" alt="">
                    <div class="titulo">
                        <span>Máquinas,<br>treinamentos<br>e tutoriais</span>
                    </div>
                </a>
                <a href="{{ route('clube.insumos') }}">
                    <div class="titulo">
                        <span>Insumos e<br>acessórios</span>
                    </div>
                    <img src="{{ asset('assets/img/layout/clube-insumos.jpg') }}" alt="">
                </a>
            </div>
        </div>
    </div>

@endsection
