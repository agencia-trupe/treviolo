@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="clube-maquinas-margin center" style="background:#FFF">
            <div class="clube-container clube-maquinas">
                <a href="{{ route('clube') }}" class="btn-voltar">VOLTAR</a>
                <a href="{{ route('clube.checkout') }}" class="btn-sacola"><span>SACOLA DE COMPRAS</span></a>

                <div class="titulo">
                    <h1>Máquinas</h1>
                    <p>As nossas máquinas superautomáticas são parte essencial da arte de receber bem, encantar e crescer.<br>Com apenas um toque, a moagem dos grãos é feita na hora, preservando as características do blend.</p>
                </div>

                <div class="maquinas-selecao">
                    <a href="{{ route('clube.orcamentoMaquinas') }}">
                        <h2>Orçamento<br>de máquinas</h2>
                        <p>Entre em contato, podemos indicar a solução ideal para o seu negócio.</p>
                    </a>
                    <a href="{{ route('clube.treinamento') }}">
                        <h2>Treinamento<br>para o seu staff</h2>
                        <p>Capacitamos seus colaboradores para a experiência do espresso ser perfeita.</p>
                    </a>
                    <a href="{{ route('clube.videos') }}">
                        <h2>Vídeo<br>tutoriais</h2>
                        <p>Está com alguma dúvida? Assista nossos tutoriais em vídeo.</p>
                    </a>
                    <a href="{{ route('clube.manuais') }}">
                        <h2>Manuais<br>técnicos</h2>
                        <p>Precisa de informação técnica em PDF? Está na mão! Clique aqui.</p>
                    </a>
                </div>

                <a href="{{ route('contato') }}" class="btn-contato">Assistência técnica</a>
            </div>
        </div>
    </div>

@endsection
