@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="center" style="background:#FFF">
            <div class="clube-container clube-cadastro">
                <a href="{{ route('clube.login') }}" class="btn-voltar">VOLTAR</a>

                <div class="titulo">
                    <h1>Dados cadastrais</h1>
                    <p>Você só precisa criar uma conta para ter acesso<br>a todos os serviços e produtos da Treviolo.</p>
                </div>

                <form action="{{ route('clube.cadastroPost') }}" method="POST">
                    {!! csrf_field() !!}

                    <div class="selecao-cadastro">
                        <label>
                            <input type="radio" name="cadastro" value="pessoa_fisica" @if(old('cadastro') === 'pessoa_fisica' || old('cadastro') == null) checked @endif>
                            <span>PESSOA FÍSICA</span>
                        </label>
                        <label>
                            <input type="radio" name="cadastro" value="pessoa_juridica" @if(old('cadastro') == 'pessoa_juridica') checked @endif>
                            <span>PESSOA JURÍDICA</span>
                        </label>
                    </div>

                    @if($errors->any())
                        <div class="erros">
                            @foreach($errors->all() as $error)
                            <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif

                    <div class="cadastro-pf">
                        <input type="text" name="nome" placeholder="NOME" value="{{ old('nome') }}">
                        <input type="text" name="sobrenome" placeholder="SOBRENOME" value="{{ old('sobrenome') }}">
                        <input type="text" id="cpf" name="cpf" placeholder="CPF" value="{{ old('cpf') }}">
                    </div>

                    <div class="cadastro-pj">
                        <input type="text" name="razao_social" placeholder="RAZÃO SOCIAL" value="{{ old('razao_social') }}">
                        <input type="text" name="nome_fantasia" placeholder="NOME FANTASIA" value="{{ old('nome_fantasia') }}">
                        <input type="text" id="cnpj" name="cnpj" placeholder="CNPJ" value="{{ old('cnpj') }}">
                        <input type="text" name="inscricao_estadual" placeholder="INSCRIÇÃO ESTADUAL" value="{{ old('inscricao_estadual') }}">
                        <input type="text" name="responsavel" placeholder="RESPONSÁVEL" value="{{ old('responsavel') }}">
                    </div>

                    <input type="text" id="telefone" name="telefone" placeholder="TELEFONE" value="{{ old('telefone') }}">
                    <input type="email" name="email" placeholder="E-MAIL" value="{{ old('email') }}">
                    <input type="password" name="password" placeholder="SENHA">
                    <input type="password" name="password_confirmation" placeholder="CONFIRMAR SENHA">

                    <input type="submit" value="CADASTRAR">
                </form>
            </div>
        </div>
    </div>

@endsection
