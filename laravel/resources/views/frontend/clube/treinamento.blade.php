@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="center" style="background:#FFF">
            <div class="clube-container clube-treinamento">
                <a href="{{ route('clube.maquinas') }}" class="btn-voltar">VOLTAR</a>
                <a href="{{ route('clube.checkout') }}" class="btn-sacola"><span>SACOLA DE COMPRAS</span></a>

                <div class="titulo">
                    <h1>Treinamento para o seu staff</h1>
                    <p>Transmitir o conhecimento está em nossa essência. Pensando em você e no seu negócio,<br>investimos na capacitação do seu staff para garantir sempre o melhor serviço.</p>
                </div>

                <div class="full-width">
                    <div class="left">
                        <img src="{{ asset('assets/img/treinamento/'.$treinamento->imagem_2) }}" alt="">
                    </div>
                    <div class="right">
                        <img src="{{ asset('assets/img/treinamento/'.$treinamento->imagem_1) }}" alt="">
                        <div class="texto">
                            <h2>Fale conosco e agende<br>seu treinamento Treviolo.</h2>
                            <p>{!! $treinamento->texto !!}</p>
                        </div>
                        <div class="imagem-3">
                            <img src="{{ asset('assets/img/treinamento/'.$treinamento->imagem_3) }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
