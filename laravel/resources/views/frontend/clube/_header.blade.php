<div class="clube-header">
    <div class="center">
        <h3>CLUBE TREVIOLO</h3>
        <h2>
            Esta área foi feita<br>
            para você! Entre<br>
            <strong>e fique à vontade</strong>
        </h2>
        <form action="{{ route('clube.busca') }}" method="GET">
            <input type="text" name="termo" placeholder="BUSCAR" required>
            <input type="submit" value="buscar">
        </form>
        @if(Auth::user())
        <a href="{{ route('clube.logout') }}" class="logout">FAZER LOGOUT &rarr;</a>
        @endif
    </div>
</div>
