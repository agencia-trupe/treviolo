@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="center" style="background:#FFF">
            <div class="clube-container clube-orcamento-maquinas">
                <a href="{{ route('clube.maquinas') }}" class="btn-voltar">VOLTAR</a>
                <a href="{{ route('clube.checkout') }}" class="btn-sacola"><span>SACOLA DE COMPRAS</span></a>

                @if(session('enviado'))
                <div class="titulo">
                    <h1>Confirmação de envio</h1>
                    <p>Agradecemos pela confiança. Por favor, aguarde nosso contato.</p>
                </div>
                <div class="obrigado">
                    <p>
                        Recebemos seu orçamento.<br>
                        <strong>Muito obrigado!</strong>
                    </p>
                </div>
                <a href="{{ route('contato') }}" class="btn-contato"><strong>Dúvidas?</strong> Vamos falar!</a>
                @else
                <div class="titulo">
                    <h1>Orçamento de máquinas</h1>
                    <p>Dúvidas de qual máquina é ideal para seu negócio?<br>Respondendo esse breve orçamento entraremos em contato para indicar a melhor solução para sua demanda.</p>
                </div>

                <form action="{{ route('clube.orcamentoMaquinasPost') }}" method="POST">
                    {!! csrf_field() !!}

                    @if($errors->any())
                        <div class="erros">
                            @foreach($errors->all() as $error)
                            <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif

                    <input type="text" name="nome" placeholder="NOME" value="{{ old('nome') }}">
                    <input type="text" name="sobrenome" placeholder="SOBRENOME" value="{{ old('sobrenome') }}">
                    <input type="text" id="cpf" name="cpf" placeholder="CPF" value="{{ old('cpf') }}">
                    <input type="text" id="telefone" name="telefone" placeholder="TELEFONE" value="{{ old('telefone') }}">
                    <input type="email" name="email" placeholder="E-MAIL" value="{{ old('email') }}">
                    <input type="text" name="cidade" placeholder="CIDADE" value="{{ old('cidade') }}">
                    <input type="text" name="estabelecimento" placeholder="TIPO DE ESTABELECIMENTO" value="{{ old('estabelecimento') }}">
                    <input type="text" name="pessoas" placeholder="QUANTAS PESSOAS USAM A MÁQUINA?" value="{{ old('pessoas') }}">
                    <input type="text" name="consumo" placeholder="QUAL O CONSUMO DE CAFÉ POR MÊS?" value="{{ old('consumo') }}">

                    <input type="submit" value="ENVIAR ORÇAMENTO">
                </form>
                @endif
            </div>
        </div>
    </div>

@endsection
