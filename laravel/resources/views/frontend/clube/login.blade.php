@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="center" style="background:#FFF">
            <div class="clube-login">
                <form action="{{ route('clube.loginPost') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="email" name="email" placeholder="E-MAIL" value="{{ old('email') }}" required>
                    <input type="password" name="password" placeholder="SENHA" required>

                    <div class="links">
                        <p>
                            Não é cadastrado?
                            <a href="{{ route('clube.cadastro') }}">CLIQUE AQUI</a>.
                        </p>
                        <p>
                            Esqueceu a senha?
                            <a href="{{ route('clube.esqueci') }}">CLIQUE AQUI</a>.
                        </p>
                    </div>

                    <div class="botao">
                        <input type="submit" value="ENTRAR">
                    </div>

                    @if($errors->any())
                        <div class="erros">
                            @foreach($errors->all() as $error)
                            <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

@endsection
