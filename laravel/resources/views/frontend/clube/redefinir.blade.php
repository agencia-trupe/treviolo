@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="center" style="background:#FFF">
            <div class="clube-container clube-esqueci">
                <a href="{{ route('clube.login') }}" class="btn-voltar">VOLTAR</a>

                <div class="titulo">
                    <h1>Redefinir senha</h1>
                    <p>Insira uma nova senha abaixo para redefinir seus dados de acesso.</p>
                </div>

                <form action="{{ route('clube.reset') }}" method="POST">
                    {!! csrf_field() !!}
                    @if($errors->any())
                        <div class="erros">
                            @foreach($errors->all() as $error)
                            <p>{!! $error !!}</p>
                            @endforeach
                        </div>
                    @endif
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input type="hidden" name="email" value="{{ $email }}" required>
                    <input type="password" name="password" placeholder="SENHA" required class="no-margin">
                    <input type="password" name="password_confirmation" placeholder="CONFIRMAR SENHA" required>
                    <input type="submit" value="ENVIAR">
                </form>
            </div>
        </div>
    </div>

@endsection
