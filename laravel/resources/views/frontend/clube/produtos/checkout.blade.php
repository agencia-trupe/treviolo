@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="clube-maquinas-margin center" style="background:#FFF">
            <div class="clube-container clube-produtos">
                <a href="{{ route('clube') }}" class="btn-voltar">VOLTAR</a>
                <a href="{{ route('clube.checkout') }}" class="btn-sacola"><span>SACOLA DE COMPRAS</span></a>

                <div class="titulo">
                    <h1>Checkout</h1>
                    <p>Confira seu pedido. Em breve entraremos em contato.</p>
                </div>

                @if(!count($produtos))
                <div class="nenhum-produto">
                    Nenhum produto adicionado.
                </div>
                @else
                <div class="lista-checkout">
                    <div class="header">
                        <span>Item</span>
                        <span>Quantidade</span>
                        <span>Excluir</span>
                    </div>
                    @foreach($produtos as $produto)
                    <div class="produto" data-id="{{ $produto->id }}">
                        <img src="{{ asset('assets/img/produtos/miniaturas/'.$produto->miniatura) }}" alt="">
                        <span>
                            {{ $produto->titulo }}
                            @if($produto->subtitulo)
                            &middot; {{ $produto->subtitulo }}
                            @endif
                        </span>
                        <div class="quantidade">
                            <div class="contador-wrapper" data-id="{{ $produto->id }}" data-minimo="{{ $produto->quantidade_minima }}">
                                <a href="#" class="menos"></a>
                                <input type="text" class="contador" maxlength="5" value="{{ $produto->quantidade }}">
                                <a href="#" class="mais"></a>
                            </div>
                            <a href="#" class="btn-atualizar">ATUALIZAR</a>
                        </div>
                        <a href="#" class="btn-excluir">EXCLUIR</a>
                    </div>
                    @endforeach
                </div>

                <div class="nenhum-produto mensagem-cliente-js" style="display:none">
                    Nenhum produto adicionado.
                </div>

                <form action="{{ route('clube.pedido') }}" method="POST" class="form-pedido">
                    {!! csrf_field() !!}
                    <input type="submit" value="ENVIAR PEDIDO">
                </form>
                @endif

                <a href="{{ route('contato') }}" class="btn-contato"><strong>Dúvidas?</strong> Vamos falar!</a>
            </div>
        </div>
    </div>

@endsection
