@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="clube-maquinas-margin center" style="background:#FFF">
            <div class="clube-container clube-orcamento-maquinas">
                <a href="{{ route('clube') }}" class="btn-voltar">VOLTAR</a>
                <a href="{{ route('clube.checkout') }}" class="btn-sacola"><span>SACOLA DE COMPRAS</span></a>

                <div class="titulo">
                    <h1>Confirmação de pedido</h1>
                    <p>Agradecemos pela confiança. Por favor, aguarde nosso contato.</p>
                </div>
                <div class="obrigado">
                    <p>
                        Recebemos seu pedido.<br>
                        <strong>Muito obrigado!</strong>
                    </p>
                </div>

                <a href="{{ route('contato') }}" class="btn-contato"><strong>Dúvidas?</strong> Vamos falar!</a>
        </div>
    </div>

@endsection
