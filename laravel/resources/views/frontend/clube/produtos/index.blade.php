@extends('frontend.common.template')

@section('content')

    <div class="main clube">
        @include('frontend.clube._header')

        <div class="clube-maquinas-margin center" style="background:#FFF">
            <div class="clube-container clube-produtos">
                <a href="{{ route('clube') }}" class="btn-voltar">VOLTAR</a>
                <a href="{{ route('clube.checkout') }}" class="btn-sacola"><span>SACOLA DE COMPRAS</span></a>

                <div class="titulo">
                    <h1>{{ $categoria->titulo }}</h1>
                    @if($categoria->slug === 'graos-treviolo')
                    <p>Ótima escolha! O blend Treviolo carrega<br>conhecimento, pesquisa e amor para uma<br>experiência sensorial marcante.</p>
                    @elseif($categoria->slug === 'insumos-e-acessorios')
                    <p>A Treviolo leva até você mais praticidade para apoiar<br>a sua oferta de café espresso. <strong>Faça a sua encomenda!</strong></p>
                    @endif
                </div>

                <div class="produtos-thumbs">
                    @foreach($produtos as $produto)
                    <div class="produto">
                        <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                        <h3>
                            {{ $produto->titulo }}
                            @if($produto->subtitulo)
                            <span>&middot;</span>
                            {{ $produto->subtitulo }}
                            @endif
                        </h3>
                        <p>{!! $produto->descricao !!}</p>
                        <div class="solicitar">
                            <div class="contador-wrapper" data-id="{{ $produto->id }}" data-minimo="{{ $produto->quantidade_minima }}">
                                <a href="#" class="menos"></a>
                                <input type="text" class="contador" maxlength="5" value="{{ $produto->quantidade_minima }}">
                                <a href="#" class="mais"></a>
                            </div>
                            <a href="#" class="btn-adicionar">SOLICITAR</a>
                        </div>
                        <span>Quantidade</span>
                    </div>
                    @endforeach
                </div>

                <a href="{{ route('contato') }}" class="btn-contato"><strong>Dúvidas?</strong> Vamos falar!</a>
            </div>
        </div>
    </div>

@endsection
