@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="slide @if($banner->orientacao == 'direita') slide-direita @endif">
                <div class="center">
                    <div class="texto-wrapper">
                        <div class="texto">
                            <h3>{{ $banner->subtitulo }}</h3>
                            <h2>{!! $banner->titulo !!}</h2>
                            @if($banner->link)
                            <div class="link-wrapper"><a href="{{ $banner->link }}">{{ $banner->link_texto }}</a></div>
                            @endif
                        </div>
                    </div>
                    <div class="imagem">
                        <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                    </div>
                </div>
            </div>
            @endforeach

            <a href="#" class="cycle-control cycle-prev"></a>
            <a href="#" class="cycle-control cycle-next"></a>
            <div class="cycle-pager"></div>
        </div>

        <div class="abertura">
            <div class="center">
                <img src="{{ asset('assets/img/layout/home-grao.jpg') }}" alt="">

                <div class="texto">
                    <h2>{!! $home->abertura_titulo !!}</h2>
                    <p>
                        {!! $home->abertura_texto !!}
                        <br><a href="{{ $home->abertura_link }}">CONHEÇA</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="solucoes">
            <div class="center">
                <h2>Nossas soluções</h2>

                <div>
                    @foreach($solucoes as $solucao)
                    <a href="{{ $solucao->link }}" class="solucao">
                        <div class="imagem">
                            <img src="{{ asset('assets/img/solucoes/'.$solucao->imagem) }}" alt="">
                        </div>
                        @if($solucao->novo) <span class="novo">NOVO</span> @endif
                        <div class="overlay">
                            <h3>{{ $solucao->titulo }}</h3>
                            <div class="texto">
                                <p>{{ $solucao->frase }}</p>
                                <span>DESCUBRA</span>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="clientes">
            <div class="center">
                <h2>Nossos clientes</h2>

                <div>
                    @foreach($clientes as $cliente)
                    <div class="cliente">
                        <h3>
                            {{ $cliente->nome }}<br>
                            <strong>{{ $cliente->empresa }}</strong>
                        </h3>
                        <p>{{ $cliente->depoimento }}</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="frase-bg">
            <div class="center">
                <h2>{!! $home->frase_background !!}</h2>
            </div>
        </div>

        <div class="espresso">
            <div class="center">
                <div class="texto">
                    <h3>{{ $home->subtitulo_s }}</h3>
                    <h2>{!! $home->titulo_s !!}</h2>
                    <p>{!! $home->texto_s !!}</p>
                </div>

                <div class="imagem">
                    <img src="{{ asset('assets/img/layout/home-xicara.jpg') }}" alt="">
                    <div class="letra visible"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
