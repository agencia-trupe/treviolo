@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="cabecalho" style="background-image:url('{{ asset('assets/img/cabecalhos/'.$cabecalhos->contato) }}')"></div>

        <div class="center">
            <div class="box box-left">
                <div class="texto">
                    <h3>ATENDIMENTO</h3>
                    <h2>
                        Vamos falar sobre<br>
                        o seu negócio?<br>
                        <strong>Entre em contato!</strong>
                    </h2>
                    <h4>
                        Fale com a gente, tire suas dúvidas, faça a sua sugestão.<br>
                        <strong>Você é sempre muito bem-vindo.</strong>
                    </h4>
                </div>
            </div>

            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="NOME" required>
                <input type="email" name="email" id="email" placeholder="E-MAIL" required>
                <textarea name="mensagem" id="mensagem" placeholder="SUA MENSAGEM" required></textarea>
                <div id="form-contato-response"></div>
                <button class="botao" title="ENVIAR MENSAGEM">
                    <span>ENVIAR MENSAGEM</span>
                </button>

                <img src="{{ asset('assets/img/layout/detalhe-contato.jpg') }}" alt="">
            </form>
        </div>
    </div>

@endsection
