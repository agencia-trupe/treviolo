@extends('frontend.common.template')

@section('content')

    <div class="main para-o-seu-negocio">
        <div class="cabecalho" style="background-image:url('{{ asset('assets/img/cabecalhos/'.$cabecalhos->para_o_seu_negocio) }}')"></div>

        <div class="center">
            <div class="box box-left">
                <div class="texto">
                    <h3>{{ $paraOSeuNegocio->box_subtitulo }}</h3>
                    <h2>
                        {!! $paraOSeuNegocio->box_titulo !!}
                    </h2>
                    <ul>
                        <li>PADARIAS, RESTAURANTES E HOTÉIS</li>
                        <li>ESCRITÓRIOS</li>
                    </ul>
                </div>
            </div>

            <div class="texto-wrapper">
                <div class="titulo">
                    <h2>{!! $paraOSeuNegocio->titulo_lateral !!}</h2>
                </div>
                <div class="texto">
                    {!! $paraOSeuNegocio->texto !!}
                </div>
            </div>
        </div>

        <div class="titulo-grande">
            <div class="center">
                <h2>{{ $paraOSeuNegocio->titulo_grande }}</h2>
            </div>
        </div>

        <div class="center">
            <div class="texto2-wrapper">
                <div class="texto">
                    <h4>{{ $paraOSeuNegocio->subtitulo_2 }}</h4>
                    {!! $paraOSeuNegocio->texto_2 !!}
                </div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/para-o-seu-negocio/'.$paraOSeuNegocio->imagem_1) }}" alt="">
                </div>
            </div>

            <div class="texto3-wrapper">
                <div class="texto">
                    <h4>{{ $paraOSeuNegocio->subtitulo_3 }}</h4>
                    {!! $paraOSeuNegocio->texto_3 !!}
                </div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/para-o-seu-negocio/'.$paraOSeuNegocio->imagem_2) }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection
