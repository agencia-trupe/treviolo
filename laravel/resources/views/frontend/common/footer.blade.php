    <footer class="visible">
        <div class="center">
            <div class="marca"></div>

            <div class="colunas">
                <ul class="links">
                    <li>
                        <a href="{{ route('sobre') }}">SOBRE</a>
                    </li>
                    <li>
                        <a href="{{ route('negocio') }}">PARA O SEU NEGÓCIO</a>
                    </li>
                    <li>
                        <a href="{{ route('processo') }}">PROCESSO PRODUTIVO</a>
                    </li>
                </ul>

                <div class="social">
                    @foreach(['facebook', 'instagram', 'linkedin'] as $s)
                    <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                    @endforeach
                </div>

                <ul class="links">
                    <li>
                        <a href="{{ route('nosso-blend') }}">NOSSO BLEND</a>
                    </li>
                    <li>
                        <a href="{{ route('onde-encontrar') }}">ONDE ENCONTRAR</a>
                    </li>
                    <li>
                        <a href="{{ route('contato') }}">FALE COM A GENTE</a>
                    </li>
                </ul>
            </div>

            <form action="" class="newsletter" id="form-newsletter" method="POST">
                <p>ASSINE NOSSA NEWSLETTER</p>
                <input type="email" name="email" id="newsletter_email" placeholder="NOME@EMAIL.COM.BR" required>
                <button class="botao" title="ENVIAR">
                    <span>ENVIAR</span>
                </button>
                <div id="form-newsletter-response"></div>
            </form>

            <p class="endereco">
                {{ $contato->endereco }}<br>
                {{ $contato->telefone }} &middot;
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
            </p>

            <p class="copyright">
                ©COPYRIGHT {{ date('Y') }} {{ config('site.name') }}
                <span></span>
                Todos os direitos reservados.
            </p>
        </div>
    </footer>
