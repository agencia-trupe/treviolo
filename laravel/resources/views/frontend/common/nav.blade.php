<a href="{{ route('sobre') }}" @if(tools::isactive('sobre')) class="active" @endif>sobre</a>
<a href="{{ route('negocio') }}" @if(tools::isactive('negocio')) class="active" @endif>para o seu negócio</a>
<a href="{{ route('processo') }}" @if(tools::isactive('processo')) class="active" @endif>processo produtivo</a>
<a href="{{ route('nosso-blend') }}" @if(tools::isactive('nosso-blend')) class="active" @endif>nosso blend</a>
<a href="{{ route('onde-encontrar') }}" @if(tools::isactive('onde-encontrar')) class="active" @endif>onde encontrar</a>
<a href="{{ route('contato') }}" @if(tools::isactive('contato')) class="active" @endif>fale com a gente</a>
<a href="{{ route('clube') }}" @if(tools::isactive('clube*')) class="active" @endif>clube Treviolo</a>
