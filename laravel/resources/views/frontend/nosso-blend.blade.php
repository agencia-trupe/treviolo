@extends('frontend.common.template')

@section('content')

    <div class="main nosso-blend">
        <div class="cabecalho" style="background-image:url('{{ asset('assets/img/cabecalhos/'.$cabecalhos->nosso_blend) }}')"></div>

        <div class="center">
            <div class="box box-left">
                <div class="texto">
                    <h3>{{ $nossoBlend->box_subtitulo }}</h3>
                    <h2>
                        {!! $nossoBlend->box_titulo !!}
                    </h2>
                </div>
            </div>

            <div class="texto-wrapper">
                <div class="titulo">
                    <h2>{!! $nossoBlend->titulo_lateral !!}</h2>
                </div>
                <div class="texto">
                    {!! $nossoBlend->texto !!}
                </div>
            </div>

            <div class="grupo-imagens">
                <div class="left">
                    <img src="{{ asset('assets/img/nosso-blend/'.$nossoBlend->imagem_1) }}" alt="">
                    <img src="{{ asset('assets/img/nosso-blend/'.$nossoBlend->imagem_4) }}" alt="">
                </div>
                <div class="right">
                    <img src="{{ asset('assets/img/nosso-blend/'.$nossoBlend->imagem_2) }}" alt="">
                    <img src="{{ asset('assets/img/nosso-blend/'.$nossoBlend->imagem_3) }}" alt="">
                    <img src="{{ asset('assets/img/nosso-blend/'.$nossoBlend->imagem_5) }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection
