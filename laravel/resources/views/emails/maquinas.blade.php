<!DOCTYPE html>
<html>
<head>
    <title>[ORÇAMENTO DE MÁQUINAS] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Sobrenome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $sobrenome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>CPF:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cpf }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Cidade:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cidade }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Tipo de estabelecimento:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $estabelecimento }}</span>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Quantas pessoas usam a máquina:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $pessoas }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Consumo de café por mês:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $consumo }}</span><br>
</body>
</html>
