<!DOCTYPE html>
<html>
<head>
    <title>[ORÇAMENTO DE PRODUTOS] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    @if($orcamento->cliente->cadastro === 'pessoa_fisica')
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Sobrenome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->sobrenome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>CPF:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->cpf }}</span><br>
    @else
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Razão Social:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->razao_social }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome Fantasia:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->nome_fantasia }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>CNPJ:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->cnpj }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Inscrição Estadual:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->inscricao_estadual }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Responsável:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->responsavel }}</span><br>
    @endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $orcamento->cliente->email }}</span><br>
    <hr>
    <div style='color:#000;font-size:14px;font-family:Verdana;'>
        {!! $orcamento->orcamento !!}
    </div>
</body>
</html>
