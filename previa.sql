-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 18/08/2017 às 17:26
-- Versão do servidor: 5.7.17
-- Versão do PHP: 7.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `treviolo`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `subtitulo`, `titulo`, `link`, `link_texto`, `created_at`, `updated_at`) VALUES
(1, 1, 'banner1_20170818031545.jpg', 'PARA O SEU NEGÓCIO', 'Nossa especialidade:<br />\r\n<strong>tudo para voc&ecirc; servir<br />\r\no melhor caf&eacute;</strong>', 'sobre', 'DESCUBRA', '2017-08-18 03:15:45', '2017-08-18 03:15:45'),
(2, 2, 'blend_20170818031650.jpg', 'PROCESSO PRODUTIVO', 'Lorem ipsum<br />\r\n<strong>dolor sit amet</strong><br />\r\nconsectetur', 'o-processo-produtivo', 'SAIBA MAIS', '2017-08-18 03:16:50', '2017-08-18 03:17:26');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cabecalhos`
--

CREATE TABLE `cabecalhos` (
  `id` int(10) UNSIGNED NOT NULL,
  `sobre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `para_o_seu_negocio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `processo_produtivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nosso_blend` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `onde_encontrar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `cabecalhos`
--

INSERT INTO `cabecalhos` (`id`, `sobre`, `para_o_seu_negocio`, `processo_produtivo`, `nosso_blend`, `onde_encontrar`, `contato`, `created_at`, `updated_at`) VALUES
(1, 'sobre_20170818031407.jpg', 'negocio_20170818031420.jpg', 'processo_20170818031428.jpg', 'blend_20170818031429.jpg', 'encontre_20170818031439.jpg', 'fale_20170818031440.jpg', NULL, '2017-08-18 03:14:40');

-- --------------------------------------------------------

--
-- Estrutura para tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `depoimento` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `clientes`
--

INSERT INTO `clientes` (`id`, `ordem`, `nome`, `empresa`, `depoimento`, `created_at`, `updated_at`) VALUES
(1, 0, 'Roberto Lira', 'Padaria La Plaza', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pellentesque dolor eu mauris facilisis bibendum.', '2017-08-18 03:18:36', '2017-08-18 03:18:36'),
(2, 0, 'Roberto Lira', 'Padaria La Plaza', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pellentesque dolor eu mauris facilisis bibendum.', '2017-08-18 03:18:39', '2017-08-18 03:18:39'),
(3, 0, 'Roberto Lira', 'Padaria La Plaza', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pellentesque dolor eu mauris facilisis bibendum.', '2017-08-18 03:18:41', '2017-08-18 03:18:41');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `endereco`, `telefone`, `facebook`, `instagram`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, 'treviolo@treviolo.com.br', 'RUA CAETÉS, 527/541, PERDIZES · SÃO PAULO-SP', '(11) 3868 6999', 'https://www.facebook.com/treviolo/', 'https://www.instagram.com/treviolo_cafe/', '#linkedin', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `estabelecimentos`
--

CREATE TABLE `estabelecimentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_resumido` text COLLATE utf8_unicode_ci NOT NULL,
  `lat` decimal(10,7) NOT NULL,
  `lng` decimal(10,7) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `estabelecimentos`
--

INSERT INTO `estabelecimentos` (`id`, `nome`, `endereco`, `endereco_resumido`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, 'Exemplo', 'Av. Paulista, 1000, São Paulo - SP, Brasil', 'Av. Paulista, 1000', -23.5647577, -46.6518495, '2017-08-18 03:21:10', '2017-08-18 03:21:10'),
(2, 'Exemplo 2', 'Avenida Brigadeiro Faria Lima, 500 - Itaim Bibi, São Paulo - SP, Brasil', 'Av. Brigadeiro Faria Lima, 500', -23.5829280, -46.6835143, '2017-08-18 03:22:24', '2017-08-18 03:22:24');

-- --------------------------------------------------------

--
-- Estrutura para tabela `home`
--

CREATE TABLE `home` (
  `id` int(10) UNSIGNED NOT NULL,
  `abertura_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `abertura_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `abertura_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase_background` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_s` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_s` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_s` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `home`
--

INSERT INTO `home` (`id`, `abertura_titulo`, `abertura_texto`, `abertura_link`, `frase_background`, `subtitulo_s`, `titulo_s`, `texto_s`, `created_at`, `updated_at`) VALUES
(1, 'Do grão à xícara o<br><strong>processo mais harmonioso.</strong>', 'O nosso café sempre chega em formato de grãos aos clientes. Um padrão que seguimos à risca para garantir que o produto será moído na hora. O nosso blend carrega conhecimento, pesquisa e amor para uma experiência sensorial marcante.  Fazemos tudo isso para você receber bem, encantar e fazer seu negócio crescer.', '#', 'Vivemos para conhecer o café e espalhar,<br><strong>junto com os grãos, o nosso conhecimento.</strong>', 'ESPRESSO COM S?', 'Sim, café espresso<br><strong>com “s” por favor!</strong>', 'A bebida tem esse nome não por sair rápido da máquina, mas sim, eSpremida. Dessa maneira, criamos maior resistência à passagem da àgua e melhor é a extração de óleos aromáticos e outros atributos sensoriais internos do café. Espresso vem do italiano, com nome e receita padronizados no mundo todo.', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `manuais`
--

CREATE TABLE `manuais` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `manuais`
--

INSERT INTO `manuais` (`id`, `ordem`, `titulo`, `arquivo`, `created_at`, `updated_at`) VALUES
(1, 0, 'Exemplo de Manual', 'blend_20170818032254.jpg', '2017-08-18 03:22:54', '2017-08-18 03:22:54');

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_08_08_151113_create_newsletter_table', 1),
('2017_08_09_134605_create_cabecalhos_table', 1),
('2017_08_09_140758_create_banners_table', 1),
('2017_08_09_141621_create_clientes_table', 1),
('2017_08_09_141830_create_solucoes_table', 1),
('2017_08_09_142634_create_home_table', 1),
('2017_08_09_145438_create_processo_produtivo_table', 1),
('2017_08_09_150058_create_nosso_blend_table', 1),
('2017_08_09_174509_create_para_o_seu_negocio_table', 1),
('2017_08_09_175257_create_sobre_table', 1),
('2017_08_09_181039_create_estabelecimentos_table', 1),
('2017_08_16_185452_create_manuais_table', 1),
('2017_08_16_190134_create_treinamento_table', 1),
('2017_08_16_190910_create_videos_table', 1),
('2017_08_16_191627_create_orcamentos_maquinas_table', 1),
('2017_08_17_173413_create_password_resets_table', 1),
('2017_08_18_030309_create_produtos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `nosso_blend`
--

CREATE TABLE `nosso_blend` (
  `id` int(10) UNSIGNED NOT NULL,
  `box_subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `box_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_lateral` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `nosso_blend`
--

INSERT INTO `nosso_blend` (`id`, `box_subtitulo`, `box_titulo`, `titulo_lateral`, `texto`, `imagem_1`, `imagem_2`, `imagem_3`, `imagem_4`, `imagem_5`, `created_at`, `updated_at`) VALUES
(1, 'O BLEND TREVIOLO', 'Equil&iacute;brio<br />\r\ne personalidade<br />\r\ninesquec&iacute;veis', 'Este caf&eacute;<br />\r\n&eacute; Treviolo!', '<p>Blend. Como o pr&oacute;prio nome diz, uma mistura, mas n&atilde;o apenas de sabores. De sensa&ccedil;&otilde;es e experi&ecirc;ncias produzidas por uma combina&ccedil;&atilde;o de paix&atilde;o e conhecimento sobre caf&eacute;. Uma combina&ccedil;&atilde;o &uacute;nica, homog&ecirc;nea, resultado de um trabalho feito por m&atilde;os que tamb&eacute;m cuidam da natureza e conhecem profundamente a arte de compor um blend e pela tecnologia de m&aacute;quinas autom&aacute;ticas superavan&ccedil;adas.</p>\r\n', '1_20170818032042.jpg', '2_20170818032042.jpg', '3_20170818032042.jpg', '4_20170818032042.jpg', '5_20170818032042.jpg', NULL, '2017-08-18 03:20:42');

-- --------------------------------------------------------

--
-- Estrutura para tabela `orcamentos`
--

CREATE TABLE `orcamentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `orcamento` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `orcamentos_maquinas`
--

CREATE TABLE `orcamentos_maquinas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sobrenome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estabelecimento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pessoas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `consumo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `para_o_seu_negocio`
--

CREATE TABLE `para_o_seu_negocio` (
  `id` int(10) UNSIGNED NOT NULL,
  `box_subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `box_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_lateral` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_grande` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `para_o_seu_negocio`
--

INSERT INTO `para_o_seu_negocio` (`id`, `box_subtitulo`, `box_titulo`, `titulo_lateral`, `texto`, `titulo_grande`, `subtitulo_2`, `texto_2`, `subtitulo_3`, `texto_3`, `imagem_1`, `imagem_2`, `created_at`, `updated_at`) VALUES
(1, 'PARA O SEU NEGÓCIO', 'Quando voc&ecirc; oferece<br />\r\no melhor caf&eacute;, conquista<br />\r\no paladar e a admira&ccedil;&atilde;o', 'Ofere&ccedil;a<br />\r\nTreviolo!', '<p>A Treviolo &eacute; especialista em caf&eacute; e oferece um atendimento feito por gente disposta e bem informada, al&eacute;m de alta performance em m&aacute;quinas de caf&eacute;, aux&iacute;lio no treinamento do seu staff, suporte t&eacute;cnico &aacute;gil e r&aacute;pida entrega e reposi&ccedil;&atilde;o dos produtos que voc&ecirc; precisar. Escolher a Treviolo &eacute; levar aos seus clientes um blend &uacute;nico, mas n&atilde;o s&oacute; isso: &eacute; compartilhar com eles a nossa experi&ecirc;ncia e a paix&atilde;o que todos n&oacute;s nutrimos pelo caf&eacute;.</p>\r\n', 'Temos a solução ideal para o seu negócio', 'RESTAURANTES, PADARIAS E HOTÉIS', '<h2>Ofere&ccedil;a a melhor<br />\r\n<strong>experi&ecirc;ncia</strong></h2>\r\n\r\n<p>O espresso perfeito &eacute; capaz de conquistar e de criar novos v&iacute;nculos. Seja para revenda ou para hospitalidade, oferecer um caf&eacute; de alta qualidade a seus clientes demonstra cuidado, conhecimento e paix&atilde;o pelo que se faz.</p>\r\n\r\n<h3>M&Aacute;QUINAS SUPERAUTOM&Aacute;TICAS</h3>\r\n\r\n<p>Feitas exclusivamente para o nosso blend, moem os gr&atilde;os ao simples toque de um bot&atilde;o, preservando todas as suas caracter&iacute;sticas. Praticidade e rapidez oferecidas especialmente para a revenda do seu caf&eacute; espresso ou para uma excelente hospitalidade.</p>\r\n\r\n<h3>SERVI&Ccedil;O ESPECIALIZADO</h3>\r\n\r\n<p>Na fazenda ou na cidade, temos um equipe capacitada para entender o jeito Treviolo de fazer e oferecer caf&eacute;, espalhando nossa cultura e todas as informa&ccedil;&otilde;es para que o seu caf&eacute; seja sempre perfeito.</p>\r\n', 'ESCRITÓRIO', '<h2>Leve a Treviolo<br />\r\npara o seu <strong>dia a dia</strong></h2>\r\n\r\n<p>O aroma de um Treviolo tem o poder de transformar um ambiente. E voc&ecirc; pode oferecer essa experi&ecirc;ncia diariamente aos seus clientes, colaboradores - e tamb&eacute;m a voc&ecirc; - com uma de nossas m&aacute;quinas em seu escrit&oacute;rio.</p>\r\n\r\n<h3>ESCOLHA SERVIR BEM. ESCOLHA TREVIOLO.</h3>\r\n', '1_20170818031947.jpg', '2_20170818031947.jpg', NULL, '2017-08-18 03:19:47');

-- --------------------------------------------------------

--
-- Estrutura para tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `processo_produtivo`
--

CREATE TABLE `processo_produtivo` (
  `id` int(10) UNSIGNED NOT NULL,
  `box_subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `box_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_lateral` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `processo_produtivo`
--

INSERT INTO `processo_produtivo` (`id`, `box_subtitulo`, `box_titulo`, `titulo_lateral`, `texto`, `imagem_1`, `imagem_2`, `imagem_3`, `imagem_4`, `imagem_5`, `created_at`, `updated_at`) VALUES
(1, 'PROCESSO PRODUTIVO', 'Uma jornada<br />\r\nde conhecimento<br />\r\ne amor', 'Do gr&atilde;o &agrave; x&iacute;cara<br />\r\no processo mais<br />\r\nharmonioso', '<h3>CAFEICULTURA</h3>\r\n\r\n<p>N&oacute;s participamos de todas as etapas da cria&ccedil;&atilde;o do blend: desde a produ&ccedil;&atilde;o at&eacute; a colheita e beneficiamento dos gr&atilde;os, que colhidos, secados e beneficiados no est&aacute;gio certo de matura&ccedil;&atilde;o concentram acidez, sacarose, &oacute;leos, aromas e sabores na medida certa.</p>\r\n\r\n<h3>BLENDS DO CAF&Eacute; ESPRESSO TREVIOLO</h3>\r\n\r\n<p>O nosso blend est&aacute; intimamente ligado &agrave; ideia de acolher e cativar. Para chegar a ele, mais de 40 variedades de caf&eacute; origin&aacute;rias do Brasil, da It&aacute;lia e de regi&otilde;es africanas j&aacute; foram plantadas na nossa fazenda e em fazendas parceiras.</p>\r\n\r\n<h3>CAF&Eacute; MO&Iacute;DO NA HORA</h3>\r\n\r\n<p>A experi&ecirc;ncia s&oacute; &eacute; plena quando o caf&eacute; &eacute; mo&iacute;do na hora em m&aacute;quinas de alta tecnologia, feitas exclusivamente para aqueles gr&atilde;os. Um convite a quem quiser entrar, se sentar e se conectar com os pr&oacute;prios sentidos.</p>\r\n', '1_20170818032016.jpg', '2_20170818032016.jpg', '3_20170818032016.jpg', '4_20170818032016.jpg', '5_20170818032016.jpg', NULL, '2017-08-18 03:20:16');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(10) UNSIGNED NOT NULL,
  `produtos_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `miniatura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `quantidade_minima` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtos`
--

INSERT INTO `produtos` (`id`, `produtos_categoria_id`, `ordem`, `capa`, `miniatura`, `titulo`, `subtitulo`, `descricao`, `quantidade_minima`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'solucao1_20170818172623.jpg', 'solucao1_20170818172623.jpg', 'Exemplo', '1kg', '<strong>Caf&eacute; torrado em gr&atilde;os<br />\r\nTorra m&eacute;dia</strong><br />\r\nQtdade m&iacute;nima 10 pacotes', 10, '2017-08-18 17:26:23', '2017-08-18 17:26:23');

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos_categorias`
--

CREATE TABLE `produtos_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `produtos_categorias`
--

INSERT INTO `produtos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'Grãos Treviolo', 'graos-treviolo', NULL, NULL),
(2, 1, 'Insumos e acessórios', 'insumos-e-acessorios', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `sobre`
--

CREATE TABLE `sobre` (
  `id` int(10) UNSIGNED NOT NULL,
  `box_subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `box_titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_inferior` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_4` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `sobre`
--

INSERT INTO `sobre` (`id`, `box_subtitulo`, `box_titulo`, `texto`, `titulo_inferior`, `subtitulo_2`, `titulo_2`, `texto_2`, `subtitulo_3`, `texto_3`, `subtitulo_4`, `texto_4`, `imagem_1`, `imagem_2`, `created_at`, `updated_at`) VALUES
(1, 'PROPÓSITO', 'Paix&atilde;o e conhecimento:<br />\r\n<strong>o segredo do blend<br />\r\nperfeito para voc&ecirc;</strong>', '<p>Caf&eacute; &eacute; encontro, &eacute; instante, &eacute; paix&atilde;o.<br />\r\n&Eacute; uma pausa, um presente. Caf&eacute; &eacute; inspira&ccedil;&atilde;o.<br />\r\nE o nosso caf&eacute; vem do amor que temos por aquilo<br />\r\nque fazemos e pelo ritual de aroma e sabor que<br />\r\npodemos proporcionar &agrave;s pessoas.<br />\r\nCaf&eacute; &eacute; compartilhamento, &eacute; experi&ecirc;ncia.<br />\r\n&Eacute; uma troca de ideias e de sabedoria.<br />\r\nNo caf&eacute;, h&aacute; conhecimento.<br />\r\nE &eacute; isso o que queremos espalhar para voc&ecirc;,<br />\r\njunto de nossos gr&atilde;os.</p>\r\n', 'Vivemos para conhecer o caf&eacute;<br />\r\ne espalhar, junto com os gr&atilde;os,<br />\r\no nosso conhecimento.', 'HISTÓRIA', 'Nossas d&eacute;cadas de<br />\r\n<strong>dedica&ccedil;&atilde;o e de sabor</strong>', '<p>A Treviolo nasceu em 1992, mas muito anos antes a nossa fam&iacute;lia j&aacute; se dedicava &agrave; sua maior paix&atilde;o: a cafeicultura.</p>\r\n\r\n<h3>AMOR &Agrave; TERRA, AO PLANTIO, AOS GR&Atilde;OS<br />\r\nE AO CONHECIMENTO DO CAF&Eacute;.</h3>\r\n\r\n<p>Ensinamentos transmitidos de av&ocirc; para filhos e netos que nos levaram at&eacute; a mat&eacute;ria-prima ideal para criar o que h&aacute; de melhor em textura, acidez, aroma e sabor. Uma dedica&ccedil;&atilde;o que hoje se revela pela experi&ecirc;ncia que vem de dentro da x&iacute;cara de um Treviolo.</p>\r\n', 'QUEM SOMOS', '<h2>Somos apaixonados por fazer as pessoas se apaixonarem por caf&eacute; espresso.</h2>\r\n\r\n<p>Produtos e servi&ccedil;os, m&aacute;quinas de alta performance e profissionais 100% dedicados. Resumindo? Trabalhamos para ser o melhor servi&ccedil;o em caf&eacute; espresso que voc&ecirc; j&aacute; experimentou. Nosso compromisso &eacute; entregar o que h&aacute; de melhor para apoiar sua oferta de espressos e, assim, fazer da hospitalidade a maior aliada do seu crescimento.</p>\r\n\r\n<h3>EXIJA SEMPRE O MELHOR CAF&Eacute; EM SUAS X&Iacute;CARAS. ESCOLHA PELO SABOR.</h3>\r\n', 'CARREIRAS', '<h2>Voc&ecirc; divide essa mesma paix&atilde;o pelo caf&eacute;? Ent&atilde;o, &eacute; hora de a gente se falar</h2>\r\n\r\n<p>Queremos compartilhar nosso dia a dia com profissionais curiosos, dedicados e, acima de tudo, que tenham como seu principal foco o desejo de se dedicar ao espresso e levar &agrave;s pessoas o melhor caf&eacute; que elas j&aacute; provaram. Envie aqui seus dados, sua experi&ecirc;ncia e seus objetivos. E torne-se um membro da Treviolo.</p>\r\n', '1_20170818031923.jpg', '2_20170818031923.jpg', NULL, '2017-08-18 03:19:23');

-- --------------------------------------------------------

--
-- Estrutura para tabela `solucoes`
--

CREATE TABLE `solucoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `novo` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `solucoes`
--

INSERT INTO `solucoes` (`id`, `ordem`, `imagem`, `titulo`, `frase`, `link`, `novo`, `created_at`, `updated_at`) VALUES
(1, 1, 'solucao1_20170818031752.jpg', 'Café', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pellentesque dolor eu mauris facilisis bibendum.', '#', 0, '2017-08-18 03:17:52', '2017-08-18 03:17:52'),
(2, 2, 'solucao2_20170818031800.jpg', 'Máquinas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pellentesque dolor eu mauris facilisis bibendum.', '#', 1, '2017-08-18 03:18:01', '2017-08-18 03:18:01'),
(3, 3, 'solucao3_20170818031814.jpg', 'Insumos', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pellentesque dolor eu mauris facilisis bibendum.', '#', 0, '2017-08-18 03:18:08', '2017-08-18 03:18:14');

-- --------------------------------------------------------

--
-- Estrutura para tabela `treinamento`
--

CREATE TABLE `treinamento` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `treinamento`
--

INSERT INTO `treinamento` (`id`, `texto`, `imagem_1`, `imagem_2`, `imagem_3`, `created_at`, `updated_at`) VALUES
(1, 'Para mais informa&ccedil;&otilde;es:<br />\r\nRua Caet&eacute;s, 527/541 - Perdizes - S&atilde;o Paulo - SP<br />\r\nMande um e-mail para <a href=\"mailto:treviolo@treviolo.com.br\"><strong>treviolo@treviolo.com.br</strong></a><br />\r\nou ligue para a gente: <strong>(11) 3868 6999</strong>', '1_20170818032308.jpg', '2_20170818032308.jpg', '3_20170818032308.jpg', NULL, '2017-08-18 03:23:08');

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `acesso` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cliente',
  `cadastro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sobrenome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `razao_social` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome_fantasia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `inscricao_estadual` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `responsavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `acesso`, `cadastro`, `nome`, `sobrenome`, `cpf`, `razao_social`, `nome_fantasia`, `cnpj`, `inscricao_estadual`, `responsavel`, `telefone`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrador', '', 'trupe', '', '', '', '', '', '', '', '', 'contato@trupe.net', '$2y$10$aSNYFj38HNswdDypRWPjpea9UHxKJ3xZ0V35Po20NpQL5IA3WMGOW', 'LFT28L1yRxf3kwa4cGpXNSIFCk4T4mHm5Y94xhC4hRAVv9j3Y5YIWuPQFaMR', NULL, '2017-08-18 17:24:59');

-- --------------------------------------------------------

--
-- Estrutura para tabela `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `videos`
--

INSERT INTO `videos` (`id`, `ordem`, `titulo`, `slug`, `subtitulo`, `descricao`, `capa`, `video_tipo`, `video_codigo`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tutorial 01', 'tutorial-01', 'Exemplo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ex quam, molestie vel odio sit amet, venenatis scelerisque eros. Nulla ac euismod arcu.', '1_20170818032358.jpg', 'youtube', 'GvJyJXtDw9I', '2017-08-18 03:23:58', '2017-08-18 03:23:58'),
(2, 2, 'Tutorial 02', 'tutorial-02', 'Exemplo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ex quam, molestie vel odio sit amet, venenatis scelerisque eros. Nulla ac euismod arcu.', '3_20170818032425.jpg', 'youtube', 'rJLu43uFAX8', '2017-08-18 03:24:25', '2017-08-18 03:24:25');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `cabecalhos`
--
ALTER TABLE `cabecalhos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `estabelecimentos`
--
ALTER TABLE `estabelecimentos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `manuais`
--
ALTER TABLE `manuais`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `newsletter_email_unique` (`email`);

--
-- Índices de tabela `nosso_blend`
--
ALTER TABLE `nosso_blend`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `orcamentos`
--
ALTER TABLE `orcamentos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `orcamentos_maquinas`
--
ALTER TABLE `orcamentos_maquinas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `para_o_seu_negocio`
--
ALTER TABLE `para_o_seu_negocio`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Índices de tabela `processo_produtivo`
--
ALTER TABLE `processo_produtivo`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `produtos_produtos_categoria_id_foreign` (`produtos_categoria_id`);

--
-- Índices de tabela `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `sobre`
--
ALTER TABLE `sobre`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `solucoes`
--
ALTER TABLE `solucoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `treinamento`
--
ALTER TABLE `treinamento`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Índices de tabela `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `cabecalhos`
--
ALTER TABLE `cabecalhos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `estabelecimentos`
--
ALTER TABLE `estabelecimentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `home`
--
ALTER TABLE `home`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `manuais`
--
ALTER TABLE `manuais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `nosso_blend`
--
ALTER TABLE `nosso_blend`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `orcamentos`
--
ALTER TABLE `orcamentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `orcamentos_maquinas`
--
ALTER TABLE `orcamentos_maquinas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `para_o_seu_negocio`
--
ALTER TABLE `para_o_seu_negocio`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `processo_produtivo`
--
ALTER TABLE `processo_produtivo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `produtos_categorias`
--
ALTER TABLE `produtos_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `sobre`
--
ALTER TABLE `sobre`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `solucoes`
--
ALTER TABLE `solucoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `treinamento`
--
ALTER TABLE `treinamento`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `produtos_produtos_categoria_id_foreign` FOREIGN KEY (`produtos_categoria_id`) REFERENCES `produtos_categorias` (`id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
